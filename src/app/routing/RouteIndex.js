import { BrowserRouter, Routes, Route } from 'react-router-dom';
import LoginPage from '../pages/auth/login/LoginPage';
import RegisterPage from '../pages/auth/register/RegisterPage';
import InfoProfil from '../pages/seller/profile/InfoProfil';
import ProductPageSeller from '../pages/seller/product-page/ProductPageSeller';
import MasterLayout from '../layout/MasterLayout';
import CreateProductPage from '../pages/seller/create-product/CreateProductPage';
import CartPage from '../pages/buyer/cart/CartPage';
import ProductPageBuyer from '../pages/buyer/product-page/ProductPageBuyer';
import DaftarJualLayout from '../layout/DaftarJualLayout';
import MyProductPage from '../pages/seller/my-product/MyProductPage';
import WishlistPage from '../pages/seller/wishlist/WishlistPage';
import Home from '../pages/buyer/home/Home';
import SellerHistoryPage from '../pages/buyer/history/SellerHistoryPage';
import PublicRoutes from './PublicRoutes';
import ProtectedRoutes from './ProtectedRoutes';
import BuyerWishlistPage from '../pages/buyer/wishlist/BuyerWishlistPage';
import SoldPage from "../pages/seller/sold-page/SoldPage";
import Notification from "../layout/components/Notification";
import NotificationPage from "../pages/seller/notification/NotificationPage";
import SearchPage from "../pages/buyer/search/SearchPage";
import SettingPage from "../pages/seller/setting/SettingPage";

const RouteIndex = () => {
  return (
    <>
      <BrowserRouter>
        <Routes>
          <Route index element={<MasterLayout content={<Home />} />} />
          <Route path="" element={<PublicRoutes />}>
            <Route path="/login" element={<LoginPage />} />
            <Route path="/register" element={<RegisterPage />} />
          </Route>
          <Route path="" element={<ProtectedRoutes />}>
            <Route path="/seller">
              <Route path="notification" element={<MasterLayout content={<NotificationPage />} />} />
              <Route path="produk/:id" element={<MasterLayout content={<ProductPageSeller />} />} />
              <Route path="daftar-jual" element={<DaftarJualLayout menu1={true} content={<MyProductPage />} />} />
              <Route path="wishlist" element={<DaftarJualLayout menu2={true} content={<WishlistPage />} />} />
              <Route path="terjual" element={<DaftarJualLayout menu3={true} content={<SoldPage />} />} />
            </Route>
            <Route path="riwayat" element={<MasterLayout content={<SellerHistoryPage />} />} />
            <Route path="setting" element={<MasterLayout content={<SettingPage />} />} />
            <Route path="/info-profil" element={<MasterLayout text="Lengkapi Info Akun" content={<InfoProfil />} />} />
            <Route path="/info-produk" element={<MasterLayout content={<CreateProductPage />} />} />
            <Route path="/keranjang" element={<MasterLayout content={<CartPage />} />} />
            <Route path="/wishlist" element={<MasterLayout content={<BuyerWishlistPage />} />} />
          </Route>
          <Route path="search" element={<MasterLayout content={<SearchPage />} />} />
          <Route path="/produk/:id" element={<MasterLayout content={<ProductPageBuyer />} />} />
        </Routes>
      </BrowserRouter>
    </>
  );
};

export default RouteIndex;
