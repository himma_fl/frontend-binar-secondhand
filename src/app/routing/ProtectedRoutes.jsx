import React from 'react';

import {Navigate, Outlet} from 'react-router-dom'
import Cookies from "js-cookie";

const useAuth = () => {
    const token = Cookies.get('token')
    return !!token;
}

const ProtectedRoutes = (props) => {

    const auth = useAuth()

    return auth ? <Outlet/> : <Navigate to="/login"/>
}

export default ProtectedRoutes;