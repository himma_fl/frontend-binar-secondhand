import { Row, Col, Button, Typography, Space, Divider } from 'antd';
import { ArrowLeft } from 'phosphor-react';
import { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { deleteWishlist, getWishlist } from '../../../../setup/redux/action/WishlistAction';

const { Text } = Typography;

const BuyerWishlistPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { isLoading, data } = useSelector((state) => state.wishlist);
  const handleDelete = (wishlist_id) => {
    dispatch(deleteWishlist(wishlist_id));
  };

  useEffect(() => {
    dispatch(getWishlist());
  }, []);
  return (
    <>
      <Row style={{ marginTop: 40 }} align="center">
        <Col span={8} align="right">
          <ArrowLeft className="back-button" style={{ cursor: 'pointer', marginRight: 76 }} size={24} onClick={() => navigate('/')} />
        </Col>
        <Col xs={22} lg={8}>
          <Space direction="vertical" size={20} style={{ width: '100%' }}>
            <Text strong>Daftar Produk yang ada di wishlist kamu</Text>
            {data?.map((item) => (
              <Row>
                <Col xs={4} lg={3} className="d-flex align-items-center">
                  <img src={item?.images[0]?.image_url} alt="" style={{ width: 48, height: 48, marginTop: 4 }} />
                </Col>
                <Col span={17}>
                  <Space direction="vertical" size={4}>
                    <Text type="secondary" style={{ fontSize: 10 }}>
                      {item?.categories?.map((item) => item.name + ` `)}
                    </Text>
                    <Text>{item.name}</Text>
                    <Text>
                      {item.price?.toLocaleString('id-ID', {
                        style: 'currency',
                        currency: 'IDR',
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 0,
                      })}
                    </Text>
                  </Space>
                </Col>
                <Col xs={3} lg={4}>
                  <Text type="secondary" style={{ fontSize: 10 }}>
                    20 Apr, 14:04
                  </Text>
                </Col>
                <Col span={8} offset={16} align="right">
                  <Button
                    style={{ width: '100%', height: 36, borderRadius: 16, border: '1px solid #7126B5' }}
                    onClick={() => {
                      handleDelete(item.wishlist_id);
                    }}
                  >
                    Hapus
                  </Button>
                </Col>
                <Divider />
              </Row>
            ))}
          </Space>
        </Col>
        <Col span={8} />
      </Row>
    </>
  );
};

export default BuyerWishlistPage;
