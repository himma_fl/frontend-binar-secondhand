import React, { useEffect } from 'react';
import CasioIMG from '../../seller/product-page/img/Casio.png';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import { Navigation, Pagination, Mousewheel, Keyboard } from 'swiper';
import NextBTN from '../../seller/product-page/img/carousel button next.svg';
import PrevBTN from '../../seller/product-page/img/carousel button left.svg';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getProductByID } from '../../../../setup/redux/action/ProductByIDAction';
import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import { addWishlist } from '../../../../setup/redux/action/WishlistAction';

function ProductPageBuyer() {
  let { id } = useParams();
  const dispatch = useDispatch();
  const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;
  const { isLoading, data } = useSelector((state) => state.productByID);
  const cart = useSelector((state) => state.cart);

  const handleAddToWishlist = (id) => {
    dispatch(addWishlist(id));
  };

  const handleAddToCart = (data) => {
    console.log('ini di page buyer', data);
    dispatch({
      type: 'ADD_TO_CART',
      payload: data,
    });
  };

  useEffect(() => {
    dispatch(getProductByID(id));
  }, []);

  return (
    <>
      {isLoading ? (
        <div className="d-flex justify-content-center mt-5">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        <section id="product-page">
          <div className="container ">
            <div className="row">
              <div className="col-lg-7 col-sm-12 d-flex flex-column justify-content-lg-end align-items-lg-end">
                <div className="product-page-content">
                  <Swiper
                    breakpoints={{
                      420: {
                        pagination: false,
                        navigation: false,
                        slidesPerView: 1,
                        loop: true,
                      },
                      1920: {
                        spaceBetween: 50,
                      },
                    }}
                    modules={[Navigation, Pagination, Mousewheel, Keyboard]}
                    cssMode={true}
                    navigation={{
                      nextEl: '.custom_next ',
                      prevEl: '.custom_prev',
                    }}
                    slidesPerView={1}
                    pagination={true}
                    mousewheel={true}
                    keyboard={true}
                    loop={true}
                    className="mySwiper mt-4"
                  >
                    {data?.images?.map((item) => (
                      <SwiperSlide className="swiper-slide-buyer-product">
                        <img src={item.image_url} alt="" />
                      </SwiperSlide>
                    ))}
                  </Swiper>
                </div>
                <div className="card card-product-description mt-5">
                  <div className="card-body">
                    <h5 className="card-title">Deskripsi</h5>
                    <p className="card-text">{data.description}</p>
                  </div>
                </div>
                <div className="carousel-next-btn">
                  <img src={NextBTN} className="custom_next" style={{ height: '30px', width: '30px' }} alt="" />
                </div>
                <div className="carousel-prev-btn">
                  <img src={PrevBTN} className="custom_prev" style={{ height: '30px', width: '30px' }} alt="" />
                </div>
              </div>
              <div className="col-lg-5">
                <div className="product-page-detail">
                  <div className="card card-product-detail mt-4">
                    <div className="card-body">
                      <h5 className="card-title">{data.name}</h5>
                      <p>{data?.categories?.map((item) => item.name + ` `)}</p>
                      <h4>
                        {data.price?.toLocaleString('id-ID', {
                          style: 'currency',
                          currency: 'IDR',
                          minimumFractionDigits: 0,
                          maximumFractionDigits: 0,
                        })}
                      </h4>
                      <button
                        className="card-detail-submit-btn"
                        style={{ marginBottom: '0.625rem' }}
                        onClick={() => {
                          handleAddToCart(data);
                        }}
                      >
                        Tambah ke Keranjang
                      </button>
                      <button
                        className="card-detail-btn"
                        style={{ marginBottom: '0.625rem' }}
                        onClick={() => {
                          handleAddToWishlist(data.id);
                        }}
                      >
                        + Tambah ke Wishlist
                      </button>
                    </div>
                  </div>
                  <div class="card card-product-detail mt-lg-4">
                    <div class="card-body card-content-account">
                      <h5 class="card-title">Username</h5>
                      <p>No HP - Alamat</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
      )}
    </>
  );
}

export default ProductPageBuyer;
