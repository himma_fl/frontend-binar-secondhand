import {Col, Space, Spin, Typography} from "antd";
import React, {useEffect} from "react";
import {useNavigate, useParams} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import {searchProduct} from "../../../../../setup/redux/action/ProductAction";
import Cookies from 'js-cookie';
import {LoadingOutlined} from "@ant-design/icons";

const {Text, Title} = Typography
const antIcon = <LoadingOutlined style={{fontSize: 100}} spin/>;

const CardSearch = () => {
    const {search} = useParams()
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const {isLoading, data} = useSelector((state) => state.product);

    useEffect(() => {
        dispatch(searchProduct(Cookies.get("search")));
    }, [Cookies.get("search")]);

    const dataProduct = data

    const handleOnCardClick = (id) => {
        navigate(`/produk/${id}`)
    }

    return (
        <>
            {isLoading ? (
                    <Col span={24}>
                        <div className="d-flex justify-content-center mt-5">
                            <Spin indicator={antIcon}/>
                        </div>
                    </Col>
                )
                :
                <>
                    {dataProduct?.map((item) => (
                        <>
                            {item.sold_to === null ?
                                <Col xs={12} sm={12} md={6} lg={6} xl={6} >
                                    <div
                                        onClick={() => (handleOnCardClick(item.id))}
                                        style={{
                                            cursor: "pointer",
                                            width: "100%",
                                            height:"100%",
                                            minHeight: 209,
                                            borderRadius: 4,
                                            border: "none",
                                            boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.15)",
                                            padding: 8,
                                        }}>
                                        <Space direction="vertical" size={8} style={{width: "100%"}}>
                                            {item.images[0]?.image_url ?
                                                <img style={{
                                                    width: '100%',
                                                    height: 150,
                                                    objectFit: 'cover',
                                                }}
                                                     src={item.images[0]?.image_url}
                                                     alt=""/>
                                                :
                                                <img style={{
                                                    width: '100%',
                                                    height: 150,
                                                    objectFit: 'cover',
                                                }}
                                                     src='/product-example-2.png'
                                                     alt=""/>
                                            }

                                            <Space direction="vertical" size={4} style={{width: "100%"}}>
                                                <Title style={{fontWeight: 500, marginBottom: 0}}
                                                       level={5}>{item.name}</Title>
                                                {item?.categories?.map((item) => (
                                                    <Text type="secondary">{item.name}</Text>
                                                ))}
                                                <Title style={{fontWeight: 500}} level={5}>
                                                    {item.price?.toLocaleString('id-ID', {
                                                        style: 'currency',
                                                        currency: 'IDR',
                                                        minimumFractionDigits: 0,
                                                        maximumFractionDigits: 0,
                                                    })}
                                                </Title>
                                            </Space>
                                        </Space>
                                    </div>
                                </Col>
                                :
                                ""
                            }
                        </>
                    ))}
                </>
            }
        </>
    )
}

export default CardSearch