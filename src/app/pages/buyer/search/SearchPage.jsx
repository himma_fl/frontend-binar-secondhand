import {useParams} from "react-router-dom";
import {useEffect} from "react";
import {getProductByID} from "../../../../setup/redux/action/ProductByIDAction";
import CardSearch from "./component/CardSearch";
import {Col, Row} from "antd";

const SearchPage = () => {
    const {search} = useParams()

    useEffect(() => {

    }, []);

    return (
        <>
            <Row>
                <Col xs={0} sm={0} md={24} lg={24} xl={24}>
                    <div style={{marginTop: 48}}/>
                </Col>
            </Row>
            <Row>
                <Col xs={1} sm={1} md={4} lg={4} xl={4}/>
                <Col xs={22} sm={22} md={16} lg={16} xl={16}>
                    <Row gutter={[24, 24]} style={{marginBottom: 48}} >
                        <CardSearch/>
                    </Row>
                </Col>
                <Col xs={1} sm={1} md={4} lg={4} xl={4}/>
            </Row>
        </>
    )
}

export default SearchPage