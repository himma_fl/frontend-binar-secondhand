import { Row, Col, Button, Typography, Space, Divider } from 'antd';
import { ArrowLeft } from 'phosphor-react';
import { useDispatch, useSelector } from 'react-redux';
import { useEffect } from 'react';
import { buyProduct } from '../../../../setup/redux/action/TransactionAction';
import { Link, useNavigate } from 'react-router-dom';

const { Text } = Typography;

const CartPage = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { cart } = useSelector((state) => state.cart);

  useEffect(() => {
    console.log('ini di page cart', cart);
  }, []);

  const handleSubmit = async () => {
    let result = cart.map((item) => item.id);
    console.log(result.toString());
    await dispatch(buyProduct(result.toString(), navigate));
    await navigate('/riwayat')
    window.location.reload();
  };

  return (
    <>
      <Row style={{ marginTop: 40 }} align="center">
        <Col span={8} align="right">
          <Link to={-1}>
            <ArrowLeft className="back-button" style={{ cursor: 'pointer', marginRight: 76, color: 'black' }} size={24} />
          </Link>
        </Col>
        <Col xs={22} lg={8}>
          <Space direction="vertical" size={20} style={{ width: '100%' }}>
            <Text strong>Daftar Produk yang ada di keranjang</Text>
            {cart?.map((item) => (
              <Row>
                <Col xs={4} lg={3}>
                  <img src={item.images[0]?.image_url} alt="" style={{ width: 48, height: 48, marginTop: 4 }} />
                </Col>
                <Col span={17}>
                  <Space direction="vertical" size={4}>
                    <Text type="secondary" style={{ fontSize: 10 }}>
                      {item?.categories?.map((item) => item.name + ` `)}
                    </Text>
                    <Text>{item.name}</Text>
                    <Text>
                      {item.price?.toLocaleString('id-ID', {
                        style: 'currency',
                        currency: 'IDR',
                        minimumFractionDigits: 0,
                        maximumFractionDigits: 0,
                      })}
                    </Text>
                  </Space>
                </Col>
                <Col xs={3} lg={4}>
                  <Text type="secondary" style={{ fontSize: 10 }}>
                    20 Apr, 14:04
                  </Text>
                </Col>
                <Col span={8} offset={16} align="right">
                  <Button style={{ width: '100%', height: 36, borderRadius: 16, border: '1px solid #7126B5' }} onClick={() => dispatch({ type: 'REMOVE_CART', payload: item })}>
                    Hapus
                  </Button>
                </Col>
              </Row>
            ))}
          </Space>
          <Divider />
          <Row>
            <Col span={8} offset={8}>
              <Button
                style={{
                  width: '100%',
                  height: 36,
                  borderRadius: 16,
                  backgroundColor: '#7126B5',
                  border: 'none',
                }}
                type="primary"
                onClick={handleSubmit}
              >
                Checkout
              </Button>
            </Col>
          </Row>
        </Col>
        <Col span={8} />
      </Row>
    </>
  );
};

export default CartPage;
