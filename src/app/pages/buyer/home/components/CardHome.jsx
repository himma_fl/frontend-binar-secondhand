import { Card, Col, Spin } from 'antd';
import Meta from 'antd/lib/card/Meta';
import React, { useEffect } from 'react';
import CasioIMG from '../../../seller/product-page/img/Casio.png';
import { useDispatch, useSelector } from 'react-redux';
import { getAllProduct } from '../../../../../setup/redux/action/ProductAction';
import { LoadingOutlined } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import { ShoppingCart } from 'phosphor-react';

function CardHome() {
  const navigate = useNavigate();
  const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;
  const dispatch = useDispatch();
  const { isLoading, dataCategory } = useSelector((state) => state.product);

  const handleAddToCart = (dataCategory) => {
    dispatch({
      type: 'ADD_TO_CART',
      payload: dataCategory,
    });
  };

  useEffect(() => {
    dispatch(getAllProduct());
  }, []);

  return (
    <>
      {isLoading ? (
        <div class="mx-auto">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        <>
          {dataCategory?.map((item) => (
            <Col xs={12} sm={12} md={4} lg={4} xl={4}>
              <Card
                hoverable
                style={{
                  width: 200,
                  height: '100%',
                }}
                className="card-home"
                cover={
                  <>
                    {item.images[0]?.image_url ? (
                      <img
                        style={{
                          width: '100%',
                          height: 150,
                          objectFit: 'cover',
                          padding: 10,
                        }}
                        src={item.images[0]?.image_url}
                        alt=""
                      />
                    ) : (
                      <img
                        style={{
                          width: '100%',
                          height: 150,
                          objectFit: 'cover',
                          padding: 10,
                        }}
                        src="/product-example-2.png"
                        alt=""
                      />
                    )}
                  </>
                }
              >
                <h2 className="card-home-title">{item.name}</h2>

                <Meta description={<>{item?.categories?.map((item) => item.name + ` `)}</>} style={{ margin: '5px 0 0 -10px' }} />

                <h4 className="card-home-price">
                  {item.price?.toLocaleString('id-ID', {
                    style: 'currency',
                    currency: 'IDR',
                    minimumFractionDigits: 0,
                    maximumFractionDigits: 0,
                  })}
                </h4>
                <div className="d-flex">
                  <button className="card-home-title card-home-detail-btn" style={{ marginTop: '1rem', marginRight: '0.5rem' }} onClick={() => navigate(`/produk/${item.id}`)}>
                    Lihat Detail
                  </button>
                  <button
                    className="card-home-cart-btn"
                    style={{ marginTop: '1rem'}}
                    onClick={() => {
                      handleAddToCart(item);
                    }}
                  >
                    <ShoppingCart size={20} />
                  </button>
                </div>
              </Card>
            </Col>
          ))}
        </>
      )}
    </>
  );
}

export default CardHome;
