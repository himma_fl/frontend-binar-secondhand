import { SearchOutlined } from '@ant-design/icons';
import { Tag } from 'antd';
import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { GET_ALL_PRODUCT, GET_FILTERED_PRODUCT } from '../../../../../setup/redux/type/ProductType';

function Category() {
  const navigate = useNavigate();
  const { CheckableTag } = Tag;
  const tagsData = ['Komik', 'Pelajaran', 'Hobi'];
  const [selectedTags, setSelectedTags] = useState([]);
  const dispatch = useDispatch();
  const { data } = useSelector((state) => state.product);

  const handleChange = (tag, checked) => {
    const nextSelectedTags = checked ? [...selectedTags, tag] : selectedTags.filter((t) => t !== tag);
    setSelectedTags(nextSelectedTags);
    dispatch({
      type: `${GET_FILTERED_PRODUCT}_FULFILLED`,
      payload: data.filter(({ categories }) => new RegExp(nextSelectedTags.join('|')).test(categories[0]?.name)),
    });
  };

  return (
    <>
      {tagsData.map((tag) => (
        <CheckableTag key={tag} checked={selectedTags.indexOf(tag) > -1} onChange={(checked) => handleChange(tag, checked)} className="category">
          <SearchOutlined
            style={{
              fontSize: '20px',
              marginRight: '5px',
            }}
          />
          {tag}
        </CheckableTag>
      ))}
    </>
  );
}

export default Category;
