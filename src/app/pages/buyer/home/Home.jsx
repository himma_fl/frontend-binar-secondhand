import React, {useEffect} from 'react';
import './home.css';
import {Swiper, SwiperSlide} from 'swiper/react';
import Banner1 from './img/banner-1.png';
import Banner2 from './img/banner-2.png';
import Banner from './img/banner.png';
import 'swiper/css';
import 'swiper/css/pagination';
import {Autoplay} from 'swiper';
import {Col, Row} from 'antd';
import CardHome from './components/CardHome';
import {PlusOutlined} from '@ant-design/icons';
import Category from './components/Category';
import {useDispatch, useSelector} from 'react-redux';
import {getAllProduct} from '../../../../setup/redux/action/ProductAction';
import {useNavigate} from 'react-router-dom';

function Home() {
    const navigate = useNavigate();
    return (
        <>
            <div className="home-banners pb-5">
                <Swiper
                    breakpoints={{
                        420: {
                            watchSlidesProgress: false,
                            slidesPerView: 1,
                            spaceBetween: 90,
                        },
                        1025: {
                            slidesPerView: 1,
                            spaceBetween: 90,
                        },
                        1318: {
                            spaceBetween: 350,
                        },
                        1537: {
                            spaceBetween: 150,
                        },
                        1920: {
                            spaceBetween: 50,
                        },
                    }}
                    autoplay={{
                        delay: 2500,
                        disableOnInteraction: false,
                    }}
                    loop={true}
                    watchSlidesProgress={true}
                    slidesPerView={2}
                    centeredSlides={true}
                    spaceBetween={50}
                    modules={[Autoplay]}
                    className="BannerSwiper pt-4"
                >
                    <SwiperSlide>
                        <img src={Banner1} alt="home-banner" style={{width: '968px'}}/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={Banner} alt="home-banner" style={{width: '968px'}}/>
                    </SwiperSlide>
                    <SwiperSlide>
                        <img src={Banner2} alt="home-banner" style={{width: '968px'}}/>
                    </SwiperSlide>
                </Swiper>
            </div>
            <div className="container">
                <h5 className="fw-bold mb-4">Telusuri Kategori</h5>
                <div className="home-category">
                    <Category/>
                </div>
                <Row gutter={[32, 32]} className="pt-5 pb-5">
                    <CardHome/>
                </Row>
            </div>
            {/*<div className="home-btn-shadow" />*/}
        {/*<div className="sell-btn-group">*/}
        {/*  <button className="home-sell-btn" onClick={() => navigate('/info-produk')}>*/}
        {/*    <PlusOutlined style={{ marginRight: 10 }} /> Jual*/}
        {/*  </button>*/}
        {/*</div>*/}
        </>
    );
}

export default Home;
