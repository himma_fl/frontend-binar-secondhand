import { Button, Card, Col, Divider, Modal, Row, Space, Spin, Typography } from 'antd';
import { ArrowLeft, WhatsappLogo } from 'phosphor-react';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getTransactionHistory } from '../../../../setup/redux/action/TransactionAction';
import moment from 'moment';
import { Link, useNavigate } from 'react-router-dom';
import { LoadingOutlined } from '@ant-design/icons';

const { Text } = Typography;

const SellerHistoryPage = () => {
  const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;
  const [isModalVisible, setIsModalVisible] = useState(false);
  const navigate = useNavigate();

  const showModal = () => {
    const x = window.matchMedia('(max-width: 425px)');
    if (x.matches) {
      navigate('/');
    }
    setIsModalVisible(true);
  };

  const handleOk = () => {
    setIsModalVisible(false);
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  const dispatch = useDispatch();
  const { isLoading, data, error } = useSelector((state) => state.transaction);
  useEffect(() => {
    dispatch(getTransactionHistory());
  }, []);

  return (
    <>
      {isLoading ? (
        <div className="d-flex justify-content-center mt-5">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        <Row style={{ marginTop: 40 }} align="center">
          <Col span={8} align="right">
            <Link to={-1}>
              <ArrowLeft className="back-button" style={{ cursor: 'pointer', marginRight: 76, color: 'black' }} size={24} />
            </Link>
          </Col>
          <Col xs={22} lg={8}>
            <Space direction="vertical" size={20} style={{ width: '100%' }}>
              <Text strong>Riwayat Pembelian</Text>
              {data?.map((item) => (
                <Row>
                  {item?.products?.map((item) => (
                    <>
                      <Col span={24}>
                        <div style={{ marginBottom: 25 }}>
                          <Row>
                            <Col span={17}>
                              <Space direction="vertical" size={4}>
                                <Text type="secondary" style={{ fontSize: 10 }}>
                                  {item?.description}
                                </Text>
                                <Text>{item?.name}</Text>
                                <Text>
                                  {item.price?.toLocaleString('id-ID', {
                                    style: 'currency',
                                    currency: 'IDR',
                                    minimumFractionDigits: 0,
                                    maximumFractionDigits: 0,
                                  })}
                                </Text>
                              </Space>
                            </Col>
                            <Col xs={3} lg={4} align="right">
                              <Text type="secondary" style={{ fontSize: 10 }}>
                                {moment(item.updatedAt).format('DD MMM, YYYY')}
                              </Text>
                            </Col>
                          </Row>
                        </div>
                      </Col>
                    </>
                  ))}
                  <Col span={16} offset={8}>
                    <Row gutter={16}>
                      <Col span={12} offset={12}>
                        <Button style={{ width: '100%', height: 36, borderRadius: 16, color: 'white', backgroundColor: '#7126B5' }} onClick={showModal}>
                          Hubungi di <WhatsappLogo style={{ marginLeft: 8 }} size={18} />
                        </Button>
                        <Modal className="history-modal" footer={null} visible={isModalVisible} onOk={handleOk} onCancel={handleCancel} width={360} height={412}>
                          <div className="d-flex justify-content-center pt-4">
                            <p style={{ width: 296 }} className="text-center fw-bold pb-2">
                              Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya
                            </p>
                          </div>
                          <div className="d-flex flex-column justify-content-center align-items-center">
                            <Card
                              style={{
                                width: 300,
                                background: '#EEEEEE',
                                borderRadius: '16px',
                              }}
                            >
                              <div className="card-modal-title">
                                <p className="fw-bold text-center" style={{ fontSize: 18 }}>
                                  Produk Terjual
                                </p>
                              </div>
                              <div className="card-modal-profile">
                                <p className="fw-bold mt-3 mb-1">Nama Pembeli</p>
                                <p
                                  style={{
                                    color: '#8A8A8A',
                                    fontSize: '12px',
                                  }}
                                >
                                  Kota
                                </p>
                              </div>
                              <div className="modal-product d-flex align-items-center">
                                <img src="/product-example.png" alt="" style={{ width: 48, height: 48, marginTop: 5 }} />
                                <div className="modal-product-info ms-3">
                                  <p className="mb-0">Jam Tangan Casio</p>
                                  <p className="mb-0">Rp 200.000</p>
                                </div>
                              </div>
                            </Card>
                            <div className="modal-history-btn">
                              <Button style={{ width: 296, height: 48, borderRadius: 16, color: 'white', backgroundColor: '#7126B5', marginTop: 43 }} onClick={showModal}>
                                Hubungi via Whatsapp <WhatsappLogo style={{ marginLeft: 8 }} size={23} />
                              </Button>
                            </div>
                          </div>
                        </Modal>
                      </Col>
                    </Row>
                  </Col>
                  <Divider />
                </Row>
              ))}
            </Space>
          </Col>
          <Col span={8} />
        </Row>
      )}
    </>
  );
};

export default SellerHistoryPage;
