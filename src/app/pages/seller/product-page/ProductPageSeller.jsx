import React, { useEffect } from 'react';
import './product-page.css';
import CasioIMG from './img/Casio.png';
import { Swiper, SwiperSlide } from 'swiper/react';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import { Navigation, Pagination, Mousewheel, Keyboard } from 'swiper';
import NextBTN from './img/carousel button next.svg';
import PrevBTN from './img/carousel button left.svg';
import { useDispatch, useSelector } from 'react-redux';
import { getProfile } from '../../../../setup/redux/action/UserAction';
import { useNavigate, useParams } from 'react-router-dom';
import { deleteProduct, getProductByID } from '../../../../setup/redux/action/ProductAction';
import { LoadingOutlined } from '@ant-design/icons';
import { Col, Row, Spin, Card, Space, Typography, Skeleton } from 'antd';
import Cookies from 'js-cookie';

const { Text } = Typography;

function ProductPageSeller() {
  const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;
  const { id } = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const { dataProfile } = useSelector((state) => state.editProfile);
  const { isLoading: loadingProduct, data } = useSelector((state) => state.product);

  useEffect(() => {
    dispatch(getProductByID(id));
    dispatch(getProfile());
  }, []);

  const handleDelete = (id) => {
    console.log(id);
    dispatch(deleteProduct(id, navigate));
  };

  const handleEdit = (id) => {
    navigate('/info-produk');
    Cookies.set('edit', true);
    Cookies.set('product_id', id);
  };

  return (
    <>
      {loadingProduct ? (
        <div className="d-flex justify-content-center mt-5">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        <>
          <Row className="mt-4" align="center">
            <Col xs={24} lg={4} />
            <Col xs={22} lg={16}>
              <Row gutter={[32, 32]}>
                <Col xs={24} lg={14}>
                  <Space size={24} direction="vertical" style={{ width: '100%' }}>
                    <Skeleton className="profile-skeleton" loading={loadingProduct} active paragraph={{ rows: 1 }}>
                      <Swiper
                        modules={[Navigation, Pagination, Mousewheel, Keyboard]}
                        cssMode={true}
                        navigation={{
                          nextEl: '.custom_next ',
                          prevEl: '.custom_prev',
                        }}
                        pagination={true}
                        mousewheel={true}
                        keyboard={true}
                        loop={true}
                        style={{ height: 436, width: '100%', borderRadius: 16 }}
                        className="mySwiper"
                      >
                        {data?.images?.map((item) => (
                          <>
                            <SwiperSlide className="swiper-slide-product">
                              <div className="product-page-seller-content" style={{ position: 'relative' }}>
                                <img src={item.image_url} alt="" />
                                <Row
                                  style={{
                                    position: 'absolute',
                                    top: '50%',
                                    width: '100%',
                                    zIndex: 99,
                                  }}
                                >
                                  <Col span={12} align="left">
                                    <img
                                      className="custom_prev mx-3"
                                      src={PrevBTN}
                                      style={{
                                        position: 'relative',
                                        zIndex: 99,
                                        cursor: 'pointer',
                                        height: '30px',
                                        width: '30px',
                                        boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.15)',
                                        borderRadius: 16,
                                      }}
                                      alt=""
                                    />
                                  </Col>
                                  <Col span={12} align="right">
                                    <img
                                      className="custom_next mx-3"
                                      src={NextBTN}
                                      style={{
                                        height: '30px',
                                        width: '30px',
                                        boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.15)',
                                        borderRadius: 16,
                                        position: 'relative',
                                        zIndex: 99,
                                        cursor: 'pointer',
                                      }}
                                      alt=""
                                    />
                                  </Col>
                                </Row>
                              </div>
                            </SwiperSlide>
                          </>
                        ))}
                      </Swiper>
                    </Skeleton>
                    <Card
                      style={{
                        width: '100%',
                        boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.15)',
                        borderRadius: 16,
                      }}
                    >
                      <Space size={16} direction="vertical" style={{ width: '100%' }}>
                        <Text className="fw-bold">Deskripsi</Text>
                        <Text>{data?.description}</Text>
                      </Space>
                    </Card>
                  </Space>
                </Col>
                <Col xs={24} lg={10}>
                  <Card
                    className="mb-4"
                    style={{
                      width: '100%',
                      boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.15)',
                      borderRadius: 16,
                    }}
                  >
                    <Space size={24} direction="vertical" style={{ width: '100%' }}>
                      <Space size={16} direction="vertical" style={{ width: '100%' }}>
                        <Space size={8} direction="vertical" style={{ width: '100%' }}>
                          <Text style={{ fontSize: 16 }} className="fw-bold">
                            {data?.name}
                          </Text>
                          <Text type="secondary">{loadingProduct ? '' : data?.categories?.map((item) => item.name)}</Text>
                        </Space>
                        <Text style={{ fontSize: 16 }}>
                          {data.price?.toLocaleString('id-ID', {
                            style: 'currency',
                            currency: 'IDR',
                            minimumFractionDigits: 0,
                            maximumFractionDigits: 0,
                          })}
                        </Text>
                      </Space>
                      <Space size={14} direction="vertical" style={{ width: '100%' }}>
                        <button
                          className="card-detail-submit-btn"
                          onClick={() => {
                            handleEdit(data.id);
                          }}
                        >
                          Edit
                        </button>
                        <button
                          className="card-detail-btn"
                          onClick={() => {
                            handleDelete(data.id);
                          }}
                        >
                          Hapus
                        </button>
                      </Space>
                    </Space>
                  </Card>
                  <Card
                    className="mb-4"
                    style={{
                      width: '100%',
                      boxShadow: '0px 0px 4px 0px rgba(0,0,0,0.15)',
                      borderRadius: 16,
                    }}
                  >
                    <div className="card-body card-content-account">
                      <h5 className="card-title">{dataProfile.username}</h5>
                      <p>{dataProfile.no_hp}</p>
                      <p className="text-center">{dataProfile.alamat}</p>
                    </div>
                  </Card>
                </Col>
              </Row>
            </Col>
            <Col lg={4} />
          </Row>
        </>
      )}
    </>
  );
}

export default ProductPageSeller;
