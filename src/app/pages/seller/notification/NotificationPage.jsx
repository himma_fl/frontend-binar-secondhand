import {useDispatch, useSelector} from "react-redux";
import React, {useEffect} from "react";
import {getNotification} from "../../../../setup/redux/action/NotificationAction";
import {Skeleton, Typography, Row, Col, Card, Spin, Space, Divider} from "antd";
import {Bag} from "phosphor-react";
import moment from "moment";
import Lottie from "react-lottie";
import animationData from "../../../layout/components/no-notification.json";

const {Title, Text} = Typography

const NotificationPage = () => {
    const dispatch = useDispatch();
    const {isLoading, data} = useSelector((state) => state.notification);

    useEffect(() => {
        dispatch(getNotification());
    }, []);

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: 'xMidYMid slice',
        },
    };

    return (
        <>
            <Row style={{marginTop: 40}}>
                <Col xs={1} sm={1} md={3} lg={3} xl={3}/>
                <Col xs={22} sm={22} md={18} lg={18} xl={18}>
                    <Title level={4} className="mb-4 text-center">Notifikasi</Title>
                    <Skeleton className="profile-skeleton" loading={isLoading} active
                              paragraph={{rows: 1}}>
                        {data.length !== 0 ?
                            <>
                                {data?.map((item, index) => (
                                    <div>
                                        <Space direction="vertical" size={20} style={{width: '100%'}}>
                                            <Row>
                                                <Col span={18}>
                                                    <Space direction="vertical" size={0}>
                                                        <Text type="secondary" style={{fontSize: 10}}>
                                                            <Bag size={12}/> Produk Terjual
                                                        </Text>
                                                        <Text className="fw-bold">{item.name}</Text>
                                                        <Text>terjual oleh {item.sold_to.username}</Text>
                                                    </Space>
                                                </Col>
                                                <Col span={5} align="right">
                                                    <Text type="secondary" style={{fontSize: 10}}>
                                                        {moment(item.sold_at).format('DD MMM, YYYY')}
                                                    </Text>
                                                </Col>
                                                <>
                                                    <Col span={1}>
                                                        {item.is_notif_read === true ? (
                                                            ''
                                                        ) : (
                                                            <Space direction="vertical" size={0}>
                                                                <div className="notification-ellipse"/>
                                                            </Space>
                                                        )}
                                                    </Col>
                                                </>
                                            </Row>
                                        </Space>
                                        {index === data.length - 1 ? "" : <Divider/>}
                                    </div>
                                ))}
                                <div style={{marginBottom: 98}}/>
                            </>
                            :
                            <>
                                <Lottie
                                    options={defaultOptions}
                                    height={300}
                                    width={300}
                                />
                                <Text className="text-center">Tidak ada notifikasi</Text>
                            </>
                        }
                    </Skeleton>
                </Col>
                <Col xs={1} sm={1} md={3} lg={3} xl={3}/>
            </Row>
        </>
    )
}

export default NotificationPage