import { LoadingOutlined } from '@ant-design/icons';
import './info-profil.css';
import React, { useEffect, useState } from 'react';
import { Col, Form, Input, Row, Space, Spin } from 'antd';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { editProfile, getProfile } from '../../../../setup/redux/action/UserAction';
import { GET_PROFILE } from '../../../../setup/redux/type/UserType';
import { ArrowLeft } from 'phosphor-react';

const { TextArea } = Input;

function InfoProfil() {
  const antIcon = <LoadingOutlined style={{ fontSize: 100 }} spin />;
  const dispatch = useDispatch();
  const { isLoading, dataProfile } = useSelector((state) => state.editProfile);
  const navigate = useNavigate();

  const handleChange = (e) => {
    dispatch({
      type: `${GET_PROFILE}_FULFILLED`,
      payload: {
        ...dataProfile,
        [e.target.name]: e.target.value,
      },
    });
  };

  const onFinish = () => {
    dispatch(editProfile(dataProfile, navigate));
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  useEffect(() => {
    dispatch(getProfile());
  }, []);

  return (
    <>
      {isLoading ? (
        <div className="d-flex justify-content-center mt-5">
          <Spin indicator={antIcon} />
        </div>
      ) : (
        <Row style={{ marginTop: 40 }} justify={'center'}>
          <Col span={8} align="right">
            <ArrowLeft className="back-button" style={{ cursor: 'pointer', marginRight: 76 }} size={24} onClick={() => navigate('/')} />
          </Col>
          <Col xs={22} s={24} lg={8}>
            <Space direction="vertical" size={20} style={{ width: '100%' }}>
              <Form
                name="infoprofil"
                labelCol={{
                  span: 24,
                }}
                wrapperCol={{
                  span: 24,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
              >
                <p>Username</p>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: 'Username tidak boleh kosong',
                    },
                  ]}
                >
                  <Input className="input-username" placeholder="Nama" name="username" value={dataProfile?.username} onChange={handleChange} disabled />
                </Form.Item>
                <p>
                  Alamat<span style={{ color: 'red' }}>*</span>
                </p>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: 'Alamat tidak boleh kosong',
                    },
                  ]}
                >
                  <TextArea
                    autoSize={{
                      minRows: 3,
                      maxRows: 3,
                    }}
                    name="alamat"
                    value={dataProfile?.alamat}
                    onChange={handleChange}
                    maxLength={165}
                    className="input-alamat"
                    placeholder="Contoh: Jalan Ikan Hiu 33"
                  />
                </Form.Item>
                <p>
                  No Handphone<span style={{ color: 'red' }}>*</span>
                </p>
                <Form.Item
                  rules={[
                    {
                      required: true,
                      message: 'No HP tidak boleh kosong',
                    },
                  ]}
                >
                  <Input type="number" className="input-username" placeholder="Contoh: +628123123123" name="no_hp" value={dataProfile?.no_hp} onChange={handleChange} />
                </Form.Item>
                <button className="card-detail-submit-btn" htmlType="submit" block style={{ marginTop: '4rem' }}>
                  Simpan
                </button>
                <div style={{marginBottom: 98}}/>
              </Form>
            </Space>
          </Col>
          <Col span={8} />
        </Row>
      )}
    </>
  );
}

export default InfoProfil;
