import {Button, Row, Col, Typography, Space, Spin} from 'antd';
import {useEffect} from 'react';
import {getWishlishedProduct} from '../../../../setup/redux/action/ProductAction';
import {Plus, HeartStraight} from 'phosphor-react';
import {Link, useNavigate} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {LoadingOutlined} from '@ant-design/icons';

const {Title, Text} = Typography;
const antIcon = <LoadingOutlined style={{fontSize: 100}} spin/>;

const WishlistPage = () => {
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const {isLoading, dataWishlished} = useSelector((state) => state.product);

    const handleOnCardClick = (id) => {
        navigate(`/seller/produk/${id}`);
    };
    useEffect(() => {
        dispatch(getWishlishedProduct());
    }, []);
    return (
        <>
            {isLoading ? (
                <div className="d-flex justify-content-center mt-5">
                    <Spin indicator={antIcon}/>
                </div>
            ) : (
                <Row gutter={[24, 24]} style={{marginBottom: 24}} className="mb-5">
                    {dataWishlished.length !== 0 ? (
                        <>
                            {dataWishlished?.map((item) => (
                                <>
                                    <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                        <div
                                            onClick={() => handleOnCardClick(item.id)}
                                            style={{
                                                cursor: 'pointer',
                                                width: '100%',
                                                minHeight: 209,
                                                borderRadius: 4,
                                                border: 'none',
                                                boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.15)',
                                                padding: 8,
                                            }}
                                        >
                                            <Space direction="vertical" size={8} style={{width: '100%'}}>
                                                {item.images[0]?.image_url ? (
                                                    <img className="product-seller-picture"
                                                         src={item.images[0]?.image_url} alt=""/>
                                                ) : (
                                                    <img className="product-seller-picture"
                                                         src="/product-example-2.png" alt=""/>
                                                )}

                                                <Space direction="vertical" size={4} style={{width: '100%'}}>
                                                    <Title style={{fontWeight: 500, marginBottom: 0}} level={5}>
                                                        {item.name}
                                                    </Title>
                                                    {item?.categories?.map((item) => (
                                                        <Text type="secondary">{item.name}</Text>
                                                    ))}
                                                    <Title style={{fontWeight: 500, marginBottom: 0}} level={5}>
                                                        {item.price?.toLocaleString('id-ID', {
                                                            style: 'currency',
                                                            currency: 'IDR',
                                                            minimumFractionDigits: 0,
                                                            maximumFractionDigits: 0,
                                                        })}
                                                    </Title>
                                                    <Title style={{fontWeight: 500}} level={5}>
                                                        <p className="d-flex align-items-center justify-content-end"
                                                           style={{fontSize: '12px'}}>
                                                            {item.wishlist_count} <HeartStraight size={14}
                                                                                                 style={{marginLeft: 5}}/>
                                                        </p>
                                                    </Title>
                                                </Space>
                                            </Space>
                                        </div>
                                    </Col>
                                </>
                            ))}
                        </>
                    ) : (
                        <>
                            <Col span={24} align="middle">
                                <img className="mt-4" style={{height: 194, width: 276, marginBottom: 24}}
                                     src="/no-product-example.svg" alt=""/>
                                <Title className="menu-list" style={{fontWeight: 500}} level={5}>
                                    Belum ada produkmu yang diminati nih, sabar ya rejeki nggak kemana kok
                                </Title>
                            </Col>
                        </>
                    )}
                </Row>
            )}
        </>
    );
};

export default WishlistPage;
