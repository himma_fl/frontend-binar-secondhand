import {Divider, Steps} from "antd";
const { Step } = Steps;

const StepsForm = (props) => {
    return(
        <>
            <div className="mb-5">
                <Steps size="small" current={props.current}>
                    <Step title="Detail Produk" />
                    <Step title="Input Gambar" />
                </Steps>
            </div>
            <Divider/>
        </>
    )
}

export default StepsForm