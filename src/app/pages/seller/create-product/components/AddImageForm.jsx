import {Button, Form, Modal, Row, Col, Upload} from "antd";
import {LoadingOutlined, PlusOutlined} from "@ant-design/icons";
import React, {useEffect, useState} from "react";
import {addImage, createProduct} from "../../../../../setup/redux/action/ProductAction";
import {ArrowRight, Trash, X} from "phosphor-react";
import {useDispatch, useSelector} from "react-redux";
import {useNavigate} from "react-router-dom";
import Cookies from "js-cookie";
import {
    DELETE_PRODUCT_IMAGE,
    GET_PRODUCT_BY_ID,
    POST_PRODUCT,
    PRODUCT
} from "../../../../../setup/redux/type/ProductType";
import {getCategory} from "../../../../../setup/redux/action/CategoryAction";

const getBase64 = (file) =>
    new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = () => resolve(reader.result);

        reader.onerror = (error) => reject(error);
    });

const AddImageForm = () => {
    const dispatch = useDispatch()
    const navigate = useNavigate()
    const [previewVisible, setPreviewVisible] = useState(false);
    const [previewImage, setPreviewImage] = useState('');
    const [previewTitle, setPreviewTitle] = useState('');
    const [fileList, setFileList] = useState([]);
    const {isLoading: newImageLoading, data, dataImage} = useSelector((state) => state.product);

    useEffect(() => {
        dispatch({type: `${PRODUCT}_LOADING_FULFILLED`});
    }, []);

    const handleCancel = () => setPreviewVisible(false);

    const handlePreview = async (file) => {
        if (!file.url && !file.preview) {
            file.preview = await getBase64(file.originFileObj);
        }

        setPreviewImage(file.url || file.preview);
        setPreviewVisible(true);
        setPreviewTitle(file.name || file.url.substring(file.url.lastIndexOf('/') + 1));
    };

    const handleChange = ({fileList: newFileList}) => setFileList(newFileList);

    const uploadButton = (
        <div>
            <PlusOutlined/>
            <div
                style={{
                    marginTop: 8,
                }}
            >
                Upload
            </div>
        </div>
    );

    const onFinish = async () => {
        let result = fileList.map(a => a.originFileObj);
        console.log(result)
        console.log(result.length)
        for (let i = 0; i < result.length; i++) {
            let formData = new FormData();
            formData.append('images', result[i])
            formData.append('product_id', Cookies.get('product_id').toString())
            await dispatch(addImage(formData, navigate))
        }
        dispatch({type: `${PRODUCT}_LOADING_FULFILLED`});
        await navigate('/seller/daftar-jual')
        window.location.reload();
    };

    const onFinishFailed = (errorInfo) => {

    };

    const handleDeleteImage = async (id) => {
        console.log(dataImage)
        let afterDelete = data.images.filter((item) => item.id !== id)
        console.log(afterDelete)
        dispatch({
            type: `${DELETE_PRODUCT_IMAGE}_FULFILLED`,
            payload: afterDelete,
        });
        // let removeIndex = data.images.map(item => item.id).indexOf(37);
        //
        // removeIndex && data.images.splice(removeIndex, 1);
        // // data.images.filter((item) => item.id !== 35)
    }

    return (
        <>
            <Form
                name="basic"
                layout="vertical"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    required={false}
                    label="Foto Produk"
                    name="username"

                    // rules={[
                    //     {
                    //         required: true,
                    //         message: 'Silahkan masukkan gambar produk!',
                    //     },
                    // ]}
                >
                    <Upload
                        // action={handleUpload}
                        listType="picture-card"
                        fileList={fileList}
                        onPreview={handlePreview}
                        onChange={handleChange}
                        beforeUpload={() => false}
                    >
                        {fileList.length >= 8 ? null : uploadButton}
                    </Upload>
                    <Modal visible={previewVisible} title={previewTitle} footer={null} onCancel={handleCancel}>
                        <img
                            alt="example"
                            style={{
                                width: '100%',
                            }}
                            src={previewImage}
                        />
                    </Modal>
                </Form.Item>

                {dataImage.length !== 0 ?
                    <Form.Item
                        required={false}
                        label="Foto Produk Sebelumnya"
                    >
                        <>
                            <Row gutter={[10, 10]}>
                                {dataImage?.map((item) => {
                                    return (
                                        <Col span={8}>
                                            <div style={{position: "relative"}}>
                                                <img
                                                    src={item.image_url}
                                                    alt=""
                                                    style={{
                                                        width: "100%",
                                                        height: 120,
                                                        objectFit: "cover",
                                                        borderRadius: 8,
                                                        boxShadow: "0 0 0 1px #CED4DA"
                                                    }}/>
                                                <Button style={{
                                                    position: "absolute",
                                                    top: 4,
                                                    right: 0,
                                                    color: "black",
                                                    padding: 4
                                                }}
                                                    onClick={() => handleDeleteImage(item.id)}
                                                        type="text"><Trash size={24}/>
                                                </Button>
                                            </div>
                                        </Col>
                                    )
                                })}
                            </Row>
                        </>
                    </Form.Item>
                    : ""
                }

                <Form.Item
                >
                    <Button loading={newImageLoading} className="mt-4" style={{
                        width: "100%",
                        height: 48,
                        borderRadius: 16,
                        backgroundColor: "#7126B5",
                        border: "none",
                        color: "white"
                    }}
                            type="primary" htmlType="submit">
                        Terbitkan <ArrowRight size={18} className="mx-1"/>
                    </Button>
                </Form.Item>
            </Form>
        </>
    )
}

export default AddImageForm