import {Button, Form, Typography, Input, Spin, Select} from "antd";
import React, {useEffect, useState} from "react";
import AddCategory from "./AddCategory";
import {ArrowRight} from "phosphor-react";
import {useDispatch, useSelector} from "react-redux";
import '../info-product.css';
import {getCategory} from "../../../../../setup/redux/action/CategoryAction";
import {POST_PRODUCT, PRODUCT} from "../../../../../setup/redux/type/ProductType";
import {createProduct, editProduct} from "../../../../../setup/redux/action/ProductAction";
import animationData from './category-loading.json';
import Lottie from "react-lottie";
import Cookies from "js-cookie";

const {TextArea} = Input
const {Option} = Select;
const {Text} = Typography

const AddProductForm = () => {
    const dispatch = useDispatch()
    const [isModalVisible, setIsModalVisible] = useState(false);
    const {step} = useSelector((state) => state.product);
    const {isLoading, dataCategory} = useSelector((state) => state.category);
    const {data, isLoading: newProductLoading} = useSelector((state) => state.product);

    useEffect(() => {
        dispatch({ type: `${PRODUCT}_LOADING_FULFILLED` });
        console.log(data)
    }, []);

    const onChange = (value) => {
        dispatch({
            type: `${POST_PRODUCT}_FULFILLED`,
            payload: {
                ...data,
                "categories": value.toString(),
            },
        });
    };

    const onSearch = (value) => {
        console.log('search:', value);
    };

    const onDropdownVisibleChange = () => {
        dispatch(getCategory());
    }

    const handleChange = (e) => {
        dispatch({
            type: `${POST_PRODUCT}_FULFILLED`,
            payload: {
                ...data,
                [e.target.name]: e.target.value,
            },
        });
    };

    const onFinish = async () => {
        console.log('Success:', data);
        data.product_id = Cookies.get("product_id");
        const obj = data;
        obj['product_id'] = obj['id'];
        delete obj['id'];

        if(Cookies.get("edit")) {
            await dispatch(editProduct(obj, step))
        } else {
            await dispatch(createProduct(data, step))
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Success:', data);
        console.log('Failed:', errorInfo);
    };

    const defaultOptions = {
        loop: true,
        autoplay: true,
        animationData: animationData,
        rendererSettings: {
            preserveAspectRatio: "xMidYMid slice"
        }
    }

    return (
        <>
            {/*<StepsForm current={0} />*/}
            <Form
                name="basic"
                layout="vertical"
                initialValues={{
                    remember: true,
                }}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="off"
            >
                <Form.Item
                    required={false}
                    label="Nama Produk"

                    rules={[
                        {
                            required: true,
                            message: 'Nama produk harus diisi!',
                        },
                    ]}
                >
                    <Input
                        className="product-input-field"
                        placeholder="Nama produk"
                        name="name"
                        value={data.name}
                        onChange={handleChange} required/>
                </Form.Item>

                <Form.Item
                    required={false}
                    label="Harga Produk"

                    rules={[
                        {
                            required: true,
                            message: 'Harga produk harus diisi!',
                        },
                    ]}
                >
                    <Input
                        type="number"
                        className="product-input-field"
                        placeholder="Harga produk"
                        name="price"
                        value={data.price}
                        onChange={handleChange} required/>
                </Form.Item>

                <Form.Item
                    required={false}
                    label="Kategori"
                    name="category"

                    rules={[
                        {
                            required: false,
                            message: 'Kategori produk harus diisi!',
                        },
                    ]}
                >
                    <Select
                        style={{
                            width: "100%",
                            borderRadius: 16,
                            border: "1px solid #D0D0D0",
                            overflow: "hidden",
                            height: 48,
                            paddingTop: 7
                        }}
                        bordered={false}
                        showSearch
                        placeholder={
                            data?.categories ? <p style={{color: "black"}}>{data.categories[0].name}</p> : "Pilih Kategori"
                        }
                        onChange={onChange}
                        onSearch={onSearch}
                        onDropdownVisibleChange={onDropdownVisibleChange}
                        filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
                    >

                        {isLoading && (
                            <div className="mx-auto" style={{backgroundColor: "white"}}>
                                <Lottie
                                    options={defaultOptions}
                                    height={50}
                                    width={50}
                                />
                            </div>
                        )}
                        {dataCategory?.map((item) => (
                            <Option value={item.id}>{item.name}</Option>
                        ))}
                    </Select>
                    <Text type="secondary" style={{fontSize: 12}}>Belum ada kategori?
                        <Text style={{color: "#7126b5"}} onClick={() => setIsModalVisible(true)}> Buat disini!</Text>
                    </Text>
                </Form.Item>

                <Form.Item
                    required={false}
                    label="Deskripsi"

                    rules={[
                        {
                            required: true,
                            message: 'Deskripsi produk harus diisi!',
                        },
                    ]}
                >
                    {/*<TextArea rows={4} />*/}
                    <TextArea
                        rows={6}
                        className="product-input-field"
                        placeholder="Deskripsi produk"
                        name="description"
                        value={data.description}
                        onChange={handleChange} required/>
                </Form.Item>

                <Form.Item
                >
                    <Button loading={newProductLoading} className="mt-4 mb-5" style={{
                        width: "100%",
                        height: 48,
                        borderRadius: 16,
                        backgroundColor: "#7126B5",
                        border: "none",
                        color: "white"
                    }}
                            type="primary" htmlType="submit">
                        Selanjutnya <ArrowRight size={18} className="mx-1"/>
                    </Button>
                    <div style={{marginBottom: 98}}/>
                </Form.Item>
            </Form>

            <AddCategory showModal={isModalVisible} close={() => setIsModalVisible(false)}/>
        </>
    )
}

export default AddProductForm