import {Select, Spin} from "antd";
import React, {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {getCategory} from "../../../../../setup/redux/action/CategoryAction";
const { Option } = Select;

const SelectField = () => {
    const dispatch = useDispatch()
    const { isLoading, dataCategory } = useSelector((state) => state.category);

    const onChange = (value) => {
        console.log(`selected ${value}`);
    };

    const onSearch = (value) => {
        console.log('search:', value);
    };

    useEffect(() => {
        dispatch(getCategory());
    }, []);

    return(
        <>
            <Select
                style={{
                    width: "100%",
                    borderRadius: 16,
                    border: "1px solid #D0D0D0",
                    overflow: "hidden",
                    height: 48,
                    paddingTop: 7
                }}
                bordered={false}
                showSearch
                placeholder="Pilih Kategori"
                optionFilterProp="children"
                onChange={onChange}
                onSearch={onSearch}
                filterOption={(input, option) => option.children.toLowerCase().includes(input.toLowerCase())}
            >
                {isLoading && (
                    <div className="mx-auto">
                        <Spin />
                    </div>
                )}
                {dataCategory?.map((item) => (
                    <Option value={item.id}>{item.name}</Option>
                ))}
            </Select>
        </>
    )
}

export default SelectField