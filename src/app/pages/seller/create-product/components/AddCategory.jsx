import {Button, Col, Input, Modal, Row} from "antd";
import {useState} from "react";
import {useDispatch} from "react-redux";
import {createCategory} from "../../../../../setup/redux/action/CategoryAction";

const AddCategory = (props) => {
    const dispatch = useDispatch()
    const [categoryName, setCategoryName] = useState("")

    const handleOk = () => {
        dispatch(createCategory(categoryName))
        setCategoryName("")
    };

    return(
        <>
            <Modal
                title={<p className="fw-bold pt-2 m-0">Tambah Kategori</p>}
                className="history-modal"
                visible={props.showModal}
                footer={null}
                onCancel={props.close}>
                <Input
                    style={{
                        width: "100%",
                        height: 48,
                        borderRadius: 16,
                        border: "1px solid #D0D0D0"
                    }}
                    onChange={(event)=>(setCategoryName(event.target.value))}
                    value={categoryName}
                    placeholder={props.placeholder} />
                <div className="d-flex justify-content-end mt-4" >
                    <Row style={{width: "100%"}} gutter={16}>
                        <Col offset={10} span={6}>
                            <Button
                                style={{width: "100%", height: 48, borderRadius: 16, border: "1px solid #7126B5"}}
                                onClick={props.close}
                            >
                                Batal
                            </Button>
                        </Col>
                        <Col span={8}>
                            <Button
                                style={{width: "100%", height: 48, borderRadius: 16, backgroundColor: "#7126B5", border: "none"}}
                                type="primary" onClick={()=>(handleOk())}
                                   >
                                Tambahkan
                            </Button>
                        </Col>
                    </Row>
                </div>
            </Modal>
        </>
    )
}

export default AddCategory