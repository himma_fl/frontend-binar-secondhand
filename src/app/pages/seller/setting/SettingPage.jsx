import {Col, Divider, Row, Typography} from "antd";
import {Link} from "react-router-dom";
import {PencilSimpleLine, SignOut} from "phosphor-react";
import Cookies from "js-cookie";

const {Title, Text} = Typography

const SettingPage = () => {
    const handleLogout = () => {
        Cookies.remove('token');
        window.location.reload();
    };

    return (
        <>
            <Title level={4} className="mb-5 text-center">Akun Saya</Title>
            <Row>
                <Col span={1}/>
                <Col span={22}>
                    <Link to='/info-profil' style={{margin: 0}}>
                        <Row className="menu-list">
                            <Col span={22}>
                                <Title className="menu-list"
                                       style={{fontWeight: 500, color: "black"}}
                                       level={5}>
                                    <PencilSimpleLine size={24} style={{marginRight: 8}}/>
                                    Ubah Akun
                                </Title>
                            </Col>
                        </Row>
                    </Link>
                    <Divider style={{marginTop: 12}}/>
                    <Row className="menu-list" onClick={handleLogout}>
                        <Col span={22}>
                            <Title className="menu-list"
                                   style={{fontWeight: 500, color: "#ff4d4f"}}
                                   level={5}>
                                <SignOut size={24} style={{marginRight: 8}}/>
                                Logout
                            </Title>
                        </Col>
                    </Row>
                    <Divider style={{marginTop: 12}}/>
                    <div className="text-center">
                        <Text type="secondary">Version 1.0.0</Text>
                    </div>
                </Col>
                <Col span={1}/>
            </Row>
        </>
    )
}

export default SettingPage