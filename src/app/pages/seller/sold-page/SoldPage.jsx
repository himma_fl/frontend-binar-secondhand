import {Button, Row, Col, Typography, Space, Spin} from "antd";
import {Plus} from "phosphor-react";
import {Link, useNavigate} from "react-router-dom";
import {useDispatch, useSelector} from "react-redux";
import React, {useEffect} from "react";
import {getMyProduct} from "../../../../setup/redux/action/ProductAction";
import {LoadingOutlined} from "@ant-design/icons";

const {Title, Text} = Typography
const antIcon = <LoadingOutlined style={{fontSize: 100}} spin/>;

const SoldPage = () => {
    const navigate = useNavigate()
    const dispatch = useDispatch()
    const {isLoading, dataMyProduct} = useSelector((state) => state.product);

    useEffect(() => {
        dispatch(getMyProduct());
    }, []);

    const dataProduct = dataMyProduct

    return (
        <>
            {isLoading ? (
                    <div className="d-flex justify-content-center mt-5">
                        <Spin indicator={antIcon}/>
                    </div>
                )
                :
                <Row gutter={[24, 24]} style={{marginBottom: 24}} className="mb-5">
                    {dataProduct?.map((item) => (
                        <>
                            {item.sold_to !== null ?
                                <Col xs={12} sm={12} md={8} lg={8} xl={8}>
                                    <div
                                        style={{
                                            cursor: "pointer",
                                            width: "100%",
                                            minHeight: "100%",
                                            borderRadius: 4,
                                            border: "none",
                                            boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.15)",
                                            padding: 8,
                                        }}>
                                        <Space direction="vertical" size={8} style={{width: "100%"}}>
                                            {item.images[0]?.image_url ?
                                                <img className="product-seller-picture"
                                                     src={item.images[0]?.image_url}
                                                     alt=""/>
                                                :
                                                <img className="product-seller-picture"
                                                     src='/product-example-2.png'
                                                     alt=""/>
                                            }
                                            <Space direction="vertical" size={4} style={{width: "100%"}}>
                                                <Title style={{fontWeight: 500, marginBottom: 0}}
                                                       level={5}>{item.name}</Title>
                                                {item?.categories?.map((item) => (
                                                    <Text type="secondary">{item.name}</Text>
                                                ))}
                                                <Title style={{fontWeight: 500}} level={5}>
                                                    {item.price?.toLocaleString('id-ID', {
                                                        style: 'currency',
                                                        currency: 'IDR',
                                                        minimumFractionDigits: 0,
                                                        maximumFractionDigits: 0,
                                                    })}
                                                </Title>
                                            </Space>
                                        </Space>
                                    </div>
                                </Col>
                                :
                                ""
                            }

                        </>
                    ))}
                </Row>
            }
        </>
    )
}

export default SoldPage