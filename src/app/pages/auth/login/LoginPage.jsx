import React, { useEffect, useState } from 'react';
import { Col, Form, Input, Row } from 'antd';
import './loginpage.css';
import BackgroundIMG from './img/Background.png';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { Link, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { authLogin } from '../../../../setup/redux/action/AuthAction';
import { getProfile } from '../../../../setup/redux/action/UserAction';

function LoginPage() {
  let navigate = useNavigate();
  const [input, setInput] = useState([]);
  const dispatch = useDispatch();
  const { isLoading, dataAuth, error } = useSelector((state) => state.auth);

  useEffect(() => {
    document.title = 'Thrifilo - Login';
  }, []);

  const handleChange = (event) => {
    dispatch({
      type: `ERROR`,
      error: null,
    });
    let value = event.target.value;
    let name = event.target.name;

    setInput({ ...input, [name]: value });
  };

  const onFinish = () => {
    dispatch(authLogin(input.username, input.password, navigate));
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <>
      <Row align="middle" justify="space-around">
        <Col lg={{ span: 12 }} md={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 24 }}>
          <h1 className="product-background-text">Thrifilo</h1>
          <img className="login-register-img" src={BackgroundIMG} alt="" />
        </Col>
        <Col lg={{ span: 12 }} md={{ span: 24 }} sm={{ span: 24 }} xs={{ span: 20 }} style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <div className="login-register-form justify-content-center" style={{ minHeight: '100vh' }}>
            <Form
              name="login"
              labelCol={{
                span: 24,
              }}
              wrapperCol={{
                span: 24,
              }}
              onFinish={onFinish}
              onFinishFailed={onFinishFailed}
            >
              <Link to="/login" className="ms-0">
                <img className="login-logo" src="/Thrifilio Logo.png" alt="" style={{ width: 100, height: '100%' }} />
              </Link>
              <h1>Masuk</h1>
              <p>Username</p>
              <Form.Item
                name="username"
                rules={[
                  {
                    required: true,
                    message: 'Username tidak boleh kosong',
                  },
                ]}
              >
                <Input className="input-login-register" placeholder="Masukkan username" name="username" value={input.username} onChange={handleChange} />
              </Form.Item>

              <p>Password</p>
              <Form.Item
                name="Password"
                rules={[
                  {
                    required: true,
                    message: 'Password tidak boleh kosong',
                  },
                ]}
              >
                <Input.Password
                  name="password"
                  value={input.password}
                  type="password"
                  onChange={handleChange}
                  className="input-login-register"
                  placeholder="Masukkan Password"
                  iconRender={(visible) => (visible ? <EyeTwoTone style={{ fontSize: '25px' }} /> : <EyeInvisibleOutlined style={{ fontSize: '25px' }} />)}
                />
              </Form.Item>
              <button className="login-btn" htmlType="submit" block style={{ marginBottom: '0.625rem' }}>
                Masuk
              </button>
            </Form>
            <p style={{ textAlign: 'center', paddingTop: '20px' }}>
              Belum punya akun?
              <a style={{ color: '#7126B5', fontWeight: '700' }} href="register">
                Daftar disini
              </a>
            </p>
          </div>
        </Col>
      </Row>
    </>
  );
}

export default LoginPage;
