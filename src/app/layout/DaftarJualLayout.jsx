import {Button, Col, Card, Row, Typography, Space, Skeleton, List} from "antd";
import {CaretRight, Cube, CurrencyDollar, HeartStraight} from "phosphor-react";
import "./layout.css"
import {Link} from "react-router-dom";
import MasterLayout from "./MasterLayout";
import {useEffect} from "react";
import {getProfile} from "../../setup/redux/action/UserAction";
import {useDispatch, useSelector} from "react-redux";

const {Title, Text} = Typography

const DaftarJualLayout = (props) => {
    const dispatch = useDispatch();
    const {isLoading: loadingProfile, dataProfile} = useSelector((state) => state.editProfile);

    useEffect(() => {
        dispatch(getProfile());
    }, []);

    const menu = [
        {
            logo: <Cube size={24} style={{marginRight: 8}}/>,
            title: 'Semua Produk',
            link: '/seller/daftar-jual',
            color: props.menu1 ? "#7126b5" : "black"
        },
        {
            logo: <HeartStraight size={24} style={{marginRight: 8}}/>,
            title: 'Diminati',
            link: '/seller/wishlist',
            color: props.menu2 ? "#7126b5" : "black"
        },
        {
            logo: <CurrencyDollar size={24} style={{marginRight: 8}}/>,
            title: 'Terjual',
            link: '/seller/terjual',
            color: props.menu3 ? "#7126b5" : "black"
        },
    ];

    return (
        <>
            <MasterLayout content={
                <Row style={{marginTop: 40}}>
                    <Col xs={1} sm={1} md={3} lg={3} xl={3}/>
                    <Col xs={22} sm={22} md={18} lg={18} xl={18}>
                        <Space direction="vertical" size={24} style={{width: "100%"}}>
                            <Skeleton className="profile-skeleton" loading={loadingProfile} active
                                      paragraph={{rows: 0}}>
                                <Col  xs={24} sm={24} md={0} lg={0} xl={0}>
                                    <Title level={4} className="text-center">Daftar Jual Saya</Title>
                                </Col>
                                <Col xs={0} sm={0} md={24} lg={24} xl={24}>
                                    <Title level={4} >Daftar Jual Saya</Title>
                                </Col>
                            </Skeleton>
                            <Card
                                style={{
                                    width: "100%",
                                    borderRadius: 16,
                                    border: "none",
                                    boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.15)"
                                }}
                            >
                                <Skeleton className="profile-skeleton" loading={loadingProfile} active
                                          paragraph={{rows: 1}}>
                                    <>
                                        <Row justify="space-around" align="middle" style={{width: "100%"}}>
                                            <Col span={12}>
                                                <Title style={{fontWeight: 500}}
                                                       level={5}>{dataProfile?.username}</Title>
                                                <Text type="secondary">{dataProfile?.alamat}</Text>
                                            </Col>
                                            <Col span={12} align="right">
                                                <Link to="/info-profil">
                                                    <Button style={{borderRadius: 8, border: "2px solid #7126B5"}}
                                                            className="edit-btn">Edit</Button>
                                                </Link>
                                            </Col>
                                        </Row>
                                    </>
                                </Skeleton>
                            </Card>
                            <Row gutter={32}>
                                <Col xs={24} sm={24} md={0} lg={0} xl={0} style={{marginBottom: 24}}>
                                    <Space size={12}>
                                        {menu?.map((item) => (
                                            <>
                                                <Link to={item.link} style={{margin: 0}}>
                                                    <Button style={{borderRadius: 16}}>
                                                        {item.title}
                                                    </Button>
                                                </Link>
                                            </>
                                        ))}
                                    </Space>
                                </Col>
                                <Col xs={0} sm={0} md={8} lg={8} xl={8}>
                                    <Card
                                        style={{
                                            width: "100%",
                                            borderRadius: 16,
                                            border: "none",
                                            boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.15)"
                                        }}
                                    >
                                        <Skeleton className="profile-skeleton" loading={loadingProfile} active
                                                  paragraph={{rows: 3}}>
                                            <Space direction="vertical" size={16} style={{width: "100%"}}>
                                                <Title style={{fontWeight: 500}} level={5}>Kategori</Title>
                                                {menu?.map((item) => (
                                                        <>
                                                            <Link to={item.link} style={{margin: 0}}>
                                                                <Row className="menu-list">
                                                                    <Col span={22}>
                                                                        <Title className="menu-list"
                                                                               style={{fontWeight: 500, color: item.color}}
                                                                               level={5}>
                                                                            {item.logo}
                                                                            {item.title}
                                                                        </Title>
                                                                    </Col>
                                                                    <Col align="right" span={2}>
                                                                        <CaretRight style={{color: item.color}} size={16}
                                                                                    className="menu-list"/>
                                                                    </Col>
                                                                </Row>
                                                            </Link>
                                                        </>
                                                    )
                                                )}

                                            </Space>
                                        </Skeleton>
                                    </Card>
                                </Col>
                                <Col xs={24} sm={24} md={16} lg={16} xl={16}>
                                    {props.content}
                                </Col>
                            </Row>
                        </Space>
                    </Col>
                    <Col xs={1} sm={1} md={3} lg={3} xl={3}/>
                </Row>
            }/>
        </>
    )
}

export default DaftarJualLayout