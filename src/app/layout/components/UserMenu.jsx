import {Button, Card, Col, Divider, Row, Skeleton, Space, Typography} from "antd";
import {Link} from "react-router-dom";
import {Bag, ClockCounterClockwise, ListBullets, SignOut, User} from "phosphor-react";
import Cookies from "js-cookie";

const {Text, Title} = Typography

const UserMenu = () => {
    const menu = [
        {
            logo: <User size={14} style={{marginRight: 8}}/>,
            title: 'Profil',
            link: '/info-profil',
            color: "black"
        },
        {
            logo: <ListBullets size={14} style={{marginRight: 8}}/>,
            title: 'Daftar Jual',
            link: '/seller/daftar-jual',
            color: "black"
        },
        {
            logo: <Bag size={14} style={{marginRight: 8}}/>,
            title: 'Wishlist',
            link: '/wishlist',
            color: "black"
        },
        {
            logo: <ClockCounterClockwise size={14} style={{marginRight: 8}}/>,
            title: 'Riwayat',
            link: '/riwayat',
            color: "black"
        },
        {
            logo: <SignOut size={14} style={{marginRight: 8}}/>,
            title: 'Logout',
            link: '/',
            color: "#ff4d4f"
        },
    ];

    const handleLogout = () => {
        Cookies.remove('token');
        window.location.reload();
    };

    return (
        <>
            <Card
                style={{
                    width: 180,
                    borderRadius: 16,
                    border: "none",
                    boxShadow: "0px 0px 10px 0px rgba(0,0,0,0.15)"
                }}
            >
                <Space direction="vertical" size={16} style={{width: "100%"}}>
                    {menu?.map((item, index) => (
                            <>
                                {item.title === "Logout" ?
                                    <>
                                        <Row className="menu-list" onClick={handleLogout}>
                                            <Col span={22}>
                                                <Text className="menu-list"
                                                      style={{fontWeight: 500, color: item.color}}
                                                      level={5}>
                                                    {item.logo}
                                                    {item.title}
                                                </Text>
                                            </Col>
                                        </Row>
                                    </>
                                    :
                                    <Link to={item.link} style={{margin: 0}}>
                                        <Row className="menu-list">
                                            <Col span={22}>
                                                <Text className="menu-list"
                                                      style={{fontWeight: 500, color: item.color}}
                                                      level={5}>
                                                    {item.logo}
                                                    {item.title}
                                                </Text>
                                            </Col>
                                        </Row>
                                    </Link>
                                }
                                {index === menu.length - 1 ? "" : <Divider style={{margin: 0, padding: 0}}/>}
                            </>
                        )
                    )}
                </Space>
            </Card>
        </>
    )
}

export default UserMenu