import { LoadingOutlined } from '@ant-design/icons';
import { Card, Col, Divider, Row, Space, Spin, Typography } from 'antd';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getNotification } from '../../../setup/redux/action/NotificationAction';
import moment from 'moment';
import Lottie from 'react-lottie';
import animationData from './no-notification.json';
import {Bag} from "phosphor-react";

const { Text } = Typography;

const Notification = () => {
  const antIcon = <LoadingOutlined style={{ fontSize: 50 }} spin />;
  const dispatch = useDispatch();
  const { isLoading, data } = useSelector((state) => state.notification);
  useEffect(() => {
    dispatch(getNotification());
  }, []);

  const defaultOptions = {
    loop: true,
    autoplay: true,
    animationData: animationData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice',
    },
  };

    return (
        <>
            {data.length !== 0 ?
                <Card
                    style={{
                        width: 376,
                        borderRadius: 16,
                        border: 'none',
                        boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.15)',
                    }}
                >
                    {isLoading && (
                        <div className="text-center">
                            <Spin indicator={antIcon}/>
                        </div>
                    )}
                    {data?.map((item, index) => (
                        <div>
                            <Space direction="vertical" size={20} style={{width: '100%'}}>
                                <Row>
                                    <Col span={18}>
                                        <Space direction="vertical" size={0}>
                                            <Text type="secondary" style={{fontSize: 10}}>
                                                <Bag size={12} /> Produk Terjual
                                            </Text>
                                            <Text className="fw-bold">{item.name}</Text>
                                            <Text>terjual oleh {item.sold_to.username}</Text>
                                        </Space>
                                    </Col>
                                    <Col span={5} align="right">
                                        <Text type="secondary" style={{fontSize: 10}}>
                                            {moment(item.sold_at).format('DD MMM, YYYY')}
                                        </Text>
                                    </Col>
                                    <>
                                        <Col span={1}>
                                            {item.is_notif_read === true ? (
                                                ''
                                            ) : (
                                                <Space direction="vertical" size={0}>
                                                    <div className="notification-ellipse"/>
                                                </Space>
                                            )}
                                        </Col>
                                    </>
                                </Row>
                            </Space>
                            {index === data.length - 1 ? "" : <Divider/>}
                        </div>
                    ))}
                </Card>
                :
                <Card
                    style={{
                        borderRadius: 16,
                        border: 'none',
                        boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.15)',
                    }}
                >
                    <Lottie
                        options={defaultOptions}
                        height={100}
                        width={100}
                    />
                    <Text className="text-center">Tidak ada notifikasi</Text>
                </Card>
            }
        </>
    );
};

export default Notification;
