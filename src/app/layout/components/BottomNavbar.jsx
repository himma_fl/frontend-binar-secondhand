import {useNavigate} from "react-router-dom";
import {useEffect, useState} from "react";
import {BellSimple, House, ListDashes, PlusCircle, User} from "phosphor-react";
import '../layout.css'

const BottomNavbar = (props) => {
    const navigate = useNavigate()

    return (
        <div className='bottom-nav' style={{backgroundColor: "white"}}>
            <div className='bn-tab'>
                    <House
                        size={32}
                        color='#000'
                        onClick={() => navigate('/')}
                    />
            </div>
            <div className='bn-tab'>
                    <BellSimple
                        size={35}
                        color='#000'
                        onClick={() => navigate('/seller/notification')}
                    />
            </div>
            <div className='bn-tab'>
                    <PlusCircle
                        size={35}
                        color='#000'
                        onClick={() => navigate('/info-produk')}
                    />
            </div>
            <div className='bn-tab'>
                    <ListDashes
                        size={35}
                        color='#000'
                        onClick={() => navigate('/seller/daftar-jual')}
                    />
            </div>
            <div className='bn-tab'>
                    <User
                        size={35}
                        color='#000'
                        onClick={() => navigate('/setting')}
                    />
            </div>
        </div>
    )
}

export default BottomNavbar