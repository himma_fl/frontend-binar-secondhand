import { Badge, Button, Dropdown, Input, Space, Tooltip, Typography, Row, Col } from 'antd';
import { BellSimple, MagnifyingGlass, SignIn, User, ShoppingCart } from 'phosphor-react';
import React, { useEffect, useState } from 'react';
import Notification from './components/Notification';
import { Link, useNavigate } from 'react-router-dom';
import Cookies from 'js-cookie';
import { getNotification, notificationRead } from '../../setup/redux/action/NotificationAction';
import { useDispatch, useSelector } from 'react-redux';
import UserMenu from './components/UserMenu';
import BottomNavbar from './components/BottomNavbar';
import { GET_ALL_PRODUCT } from '../../setup/redux/type/ProductType';

const { Text } = Typography;

const MasterLayout = (props) => {
  const navigate = useNavigate();
  const [search, setSearch] = useState('');

  const notificationMenu = (
    <>
      <Notification />
    </>
  );

  const userMenu = (
    <>
      <UserMenu />
    </>
  );

  const dispatch = useDispatch();

  const { data, isRead } = useSelector((state) => state.notification);
  const { cart } = useSelector((state) => state.cart);

  const handleNotification = () => {
    let result = data.map((item) => item.id);
    for (let i = 0; i < result.length; i++) {
      dispatch(notificationRead(result[i]));
    }
  };

  const handleSearch = () => {
    console.log(search);
    Cookies.set('search', search);
    navigate(`/search`);
  };

  useEffect(() => {
    dispatch(getNotification());
  }, []);

  return (
    <>
      <Row>
        <Col xs={0} sm={0} md={24} lg={24} xl={24}>
          <nav className="navbar bg-light" style={{ boxShadow: '0px 0px 10px 0px rgba(0,0,0,0.15)' }}>
            <div className="container-fluid" style={{ padding: '5px 141px' }}>
              <a className="navbar-brand" style={{ marginRight: 32 }} href="#">
                <Link to="/">
                  <img src="/Thrifilio Logo.png" alt="" style={{ width: '100%', height: 50 }} />
                </Link>
              </a>
              {props.text ? (
                <>
                  <Text className="d-flex mx-auto" style={{ fontWeight: 500 }}>
                    {props.text}
                  </Text>
                  <a className="navbar-brand" href="/">
                    <img src="/logo-white.png" alt="" style={{ width: 100, height: 34 }} />
                  </a>
                </>
              ) : (
                <>
                  <form className="d-flex me-auto mb-2 mb-lg-0" onSubmit={handleSearch}>
                    <Input
                      className="search-bar"
                      style={{
                        overflow: 'hidden',
                        borderRadius: 16,
                        border: 'none',
                        backgroundColor: '#EEEEEE',
                        fontSize: 14,
                        width: 444,
                        height: 48,
                      }}
                      value={search}
                      onChange={(e) => setSearch(e.target.value)}
                      suffix={<MagnifyingGlass onClick={() => handleSearch()} size={16} weight="bold" style={{ marginRight: 12, cursor: 'pointer' }} />}
                      placeholder="Cari disini ..."
                    />
                  </form>
                  {Cookies.get('token') ? (
                    <>
                      <Space size={24}>
                        <Tooltip title="Keranjang" color={'purple'}>
                          <Link to="/keranjang" style={{ margin: 10, padding: 0 }}>
                            <Badge color={'purple'} count={cart.length}>
                              <ShoppingCart className="menu-list" size={24} />
                            </Badge>
                          </Link>
                        </Tooltip>
                        <Dropdown overlay={notificationMenu} placement="bottomRight">
                          <Button style={{ margin: 10, padding: 0 }} type="link" className="menu-list" onClick={() => handleNotification()}>
                            {isRead ? (
                              <div
                                className="notification-ellipse"
                                style={{
                                  position: 'absolute',
                                  left: 29,
                                  top: 5,
                                }}
                              />
                            ) : (
                              ''
                            )}
                            <BellSimple size={24} />
                          </Button>
                        </Dropdown>
                        <Dropdown overlay={userMenu} placement="bottomRight">
                          <Button style={{ margin: 10, padding: 0 }} type="link" className="menu-list">
                            <User className="menu-list" size={24} />
                          </Button>
                        </Dropdown>
                      </Space>
                    </>
                  ) : (
                    <>
                      <Link to="/login">
                        <Button
                          type="text"
                          style={{
                            backgroundColor: '#7126B5',
                            height: 34,
                            borderRadius: 4,
                            color: 'white',
                          }}
                        >
                          <SignIn size={16} weight="bold" />
                          &nbsp;Masuk
                        </Button>
                      </Link>
                    </>
                  )}
                </>
              )}
            </div>
          </nav>
        </Col>
        <Col xs={24} sm={24} md={0} lg={0} xl={0}>
          <Row>
            <Col span={1} />
            <Col span={22}>
              {!Cookies.get('token') ? (
                <>
                  <Space style={{ width: '100%' }}>
                    <form className="d-flex me-auto mb-3 mt-3" onSubmit={handleSearch}>
                      <Input
                        className="search-bar"
                        style={{
                          overflow: 'hidden',
                          borderRadius: 16,
                          border: 'none',
                          backgroundColor: '#EEEEEE',
                          fontSize: 14,
                          width: '100%',
                          height: 48,
                        }}
                        value={search}
                        onChange={(e) => setSearch(e.target.value)}
                        suffix={<MagnifyingGlass onClick={() => handleSearch()} size={16} weight="bold" style={{ marginRight: 12, cursor: 'pointer' }} />}
                        placeholder="Cari disini ..."
                      />
                    </form>
                    <Link to="/login">
                      <Button
                        className="mb-3 mt-3"
                        type="text"
                        style={{
                          backgroundColor: '#7126B5',
                          height: 48,
                          borderRadius: 16,
                          color: 'white',
                          width: '100%',
                        }}
                      >
                        <SignIn size={16} weight="bold" />
                        &nbsp;Masuk
                      </Button>
                    </Link>
                  </Space>
                </>
              ) : (
                <form className="d-flex me-auto mb-3 mt-3" role="search" onSubmit={handleSearch}>
                  <Input
                    className="search-bar"
                    style={{
                      overflow: 'hidden',
                      borderRadius: 16,
                      border: 'none',
                      backgroundColor: '#EEEEEE',
                      fontSize: 14,
                      width: '100%',
                      height: 48,
                    }}
                    value={search}
                    onChange={(e) => setSearch(e.target.value)}
                    suffix={<MagnifyingGlass size={16} weight="bold" style={{ marginRight: 12, cursor: 'pointer' }} />}
                    placeholder="Cari disini ..."
                  />
                </form>
              )}
            </Col>
            <Col span={1} />
          </Row>
          {Cookies.get('token') ? <BottomNavbar /> : ''}
        </Col>
      </Row>
      {props.content}
    </>
  );
};

export default MasterLayout;
