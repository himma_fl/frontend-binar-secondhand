import { ADD_WISHLIST, GET_WISHLIST, DELETE_WISHLIST } from '../type/WishlistType';

const initialState = {
  data: [],
  isLoading: true,
  error: false,
};

const WishlistReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `${ADD_WISHLIST}_LOADING`:
      return {
        ...state,
      };
    case `${ADD_WISHLIST}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${ADD_WISHLIST}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    case `${GET_WISHLIST}_LOADING`:
      return {
        ...state,
      };
    case `${GET_WISHLIST}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${GET_WISHLIST}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    case `${DELETE_WISHLIST}_LOADING`:
      return {
        ...state,
      };
    case `${DELETE_WISHLIST}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${DELETE_WISHLIST}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: state.data.filter((item) => item.wishlist_id !== action.payload.wishlist_id),
      };
    default:
      return {
        ...state,
      };
  }
};

export default WishlistReducer;
