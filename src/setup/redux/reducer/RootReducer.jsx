import { combineReducers } from 'redux';
import authReducer from './AuthReducer';
import UserReducer from './UserReducer';
import NotificationReducer from './NotificationReducer';
import productByIDReducer from './ProductByIDReducer';
import ProductReducer from './ProductReducer';
import TransactionReducer from './TransactionReducer';
import WishlistReducer from './WishlistReducer';
import CategoryReducer from './CategoryReducer';
import CartReducer from './CartReducer';

export default combineReducers({
  auth: authReducer,
  product: ProductReducer,
  productByID: productByIDReducer,
  editProfile: UserReducer,
  transaction: TransactionReducer,
  notification: NotificationReducer,
  wishlist: WishlistReducer,
  category: CategoryReducer,
  cart: CartReducer,
});
