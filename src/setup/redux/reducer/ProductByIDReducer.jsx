import { GET_PRODUCT_BY_ID } from '../type/ProductByIDType';

const initialState = {
  data: [],
  isLoading: false,
  error: false,
};

const productByIDReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `${GET_PRODUCT_BY_ID}_LOADING`:
      return {
        ...state,
        isLoading: true,
        error: false,
      };
    case `${GET_PRODUCT_BY_ID}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${GET_PRODUCT_BY_ID}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default productByIDReducer;
