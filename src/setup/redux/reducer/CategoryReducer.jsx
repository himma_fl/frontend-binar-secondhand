import {CATEGORY, GET_CATEGORY, POST_CATEGORY} from "../type/CategoryType";

const initialState = {
    dataCategory: [],
    isLoading: true,
    error: false,
}

const CategoryReducer = (state = initialState, action) => {
    const {type, payload, error} = action;
    switch (type) {
        case`${CATEGORY}_LOADING`:
            return {
                ...state,
            };
        case`${GET_CATEGORY}_FULFILLED`:
            return {
                ...state,
                isLoading: false,
                dataCategory: payload
            };
        case`${POST_CATEGORY}_FULFILLED`:
            return {
                ...state,
                isLoading: false,
                dataCategory: payload
            };
        default:
            return {
                ...state,
            };
    }
}

export default CategoryReducer