import { BUY_PRODUCT, GET_TRANSACTION_HISTORY, TRANSACTION } from '../type/TransactionType';

const initialState = {
  data: [],
  isLoading: true,
  error: false,
};

const TransactionReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `${TRANSACTION}_LOADING`:
      return {
        ...state,
      };
    case `${TRANSACTION}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${GET_TRANSACTION_HISTORY}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    case `${BUY_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default TransactionReducer;
