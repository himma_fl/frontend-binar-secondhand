import { GET_PROFILE, PUT_PROFILE } from '../type/UserType';

const initialState = {
  dataProfile: [],
  isLoading: true,
  error: null,
};

const UserReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `${GET_PROFILE}_LOADING`:
      return {
        ...state,
        isLoading: true,
      };
    case `${GET_PROFILE}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${GET_PROFILE}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        dataProfile: payload,
      };
    case `${PUT_PROFILE}`:
      return {
        ...state,
        isLoading: false,
        dataProfile: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default UserReducer;
