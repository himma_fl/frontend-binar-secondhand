import { GET_NOTIFICATION, NOTIFICATION, NOTIFICATION_ISREAD } from '../type/NotificationType';

const initialState = {
  data: [],
  isRead: false,
  isLoading: true,
  error: false,
};

const NotificationReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `${NOTIFICATION}_LOADING`:
      return {
        ...state,
      };
    case `${NOTIFICATION}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `${GET_NOTIFICATION}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    case `${NOTIFICATION_ISREAD}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        isRead: payload,
      };
    default:
      return {
        ...state,
      };
  }
};

export default NotificationReducer;
