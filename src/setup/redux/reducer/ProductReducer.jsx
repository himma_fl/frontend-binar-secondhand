import {
  DELETE_PRODUCT,
  DELETE_PRODUCT_IMAGE,
  GET_ALL_PRODUCT,
  GET_FILTERED_PRODUCT,
  GET_MY_PRODUCT,
  GET_PRODUCT_BY_ID,
  GET_WISHLISHED_PRODUCT,
  POST_PRODUCT,
  PRODUCT,
  PUT_PRODUCT
} from '../type/ProductType';

const initialState = {
  data: [],
  dataImage: [],
  dataMyProduct: [],
  dataCategory: [],
  dataWishlished: [],
  step: 0,
  isLoading: true,
  error: false,
};

const ProductReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `${PRODUCT}_LOADING`:
      return {
        ...state,
        isLoading: true,
      };
    case `${PRODUCT}_LOADING_FULFILLED`:
      return {
        ...state,
        isLoading: false,
      };
    case `${PRODUCT}_ERROR`:
      return {
        ...state,
        isLoading: false,
        error: error,
      };
    case `STEP`:
      return {
        ...state,
        step: payload,
      };
    case `${GET_ALL_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
        dataCategory: payload,
      };
    case `${GET_FILTERED_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        dataCategory: payload,
      };
    case `${GET_MY_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        dataMyProduct: payload,
      };
    case `${GET_WISHLISHED_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        dataWishlished: payload,
      };
    case `${GET_PRODUCT_BY_ID}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
        dataImage: payload.images,
      };
    case `${POST_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    case `${PUT_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload,
      };
    case `${DELETE_PRODUCT}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        data: payload.filter((item) => item.id !== action.payload.id),
      };
    case `${DELETE_PRODUCT_IMAGE}_FULFILLED`:
      return {
        ...state,
        isLoading: false,
        dataImage: payload
      };
    default:
      return {
        ...state,
      };
  }
};

export default ProductReducer;
