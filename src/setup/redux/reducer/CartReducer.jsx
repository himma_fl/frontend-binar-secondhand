import { message } from 'antd';
import Cookies from 'js-cookie';

const initialState = {
  cart: [],
  isLoading: true,
  error: false,
};

const CartReducer = (state = initialState, action) => {
  const { type, payload, error } = action;
  switch (type) {
    case `ADD_TO_CART`:
      if (!Cookies.get('token')) {
        message.error('Silahkan login terlebih dahulu');
        return {
          ...state,
          error: true,
        };
      }
      message.success('Berhasil ditambahkan ke keranjang');
      return {
        ...state,
        cart: [...state.cart, payload],
      };
    case `REMOVE_CART`:
      message.success('Berhasil dihapus dari keranjang');
      return {
        ...state,
        isLoading: false,
        cart: state.cart.filter((item) => item.id !== action.payload.id),
      };
    default:
      return {
        ...state,
      };
  }
};

export default CartReducer;
