import Cookies from 'js-cookie';

export const baseUrl = 'https://binar-secondhand.herokuapp.com';

export const authUrl = baseUrl + '/auth';
export const productUrl = baseUrl + '/product';
export const userUrl = baseUrl + '/user';
export const wishlistUrl = baseUrl + '/wishlist';
export const transactionUrl = baseUrl + '/transaction';
export const categoryUrl = baseUrl + '/category';

export const headerApi = {
  Authorization: `Bearer ${Cookies.get('token')}`,
};