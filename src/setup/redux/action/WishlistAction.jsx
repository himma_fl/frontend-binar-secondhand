import { message } from 'antd';
import axios from 'axios';
import Cookies from 'js-cookie';
import { GET_WISHLIST, ADD_WISHLIST, DELETE_WISHLIST } from '../type/WishlistType';
import { wishlistUrl, headerApi } from './config';

export const getWishlist = () => async (dispatch) => {
  try {
    dispatch({ type: `${GET_WISHLIST}_LOADING` });

    await axios({
      method: 'GET',
      url: wishlistUrl + '/mine',
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
    }).then((res) => {
      dispatch({
        type: `${GET_WISHLIST}_FULFILLED`,
        payload: res.data.data,
      });
      console.log(res.data.data);
    });
  } catch (error) {
    dispatch({
      type: `${GET_WISHLIST}_ERROR`,
      error: error,
    });
  }
};

export const addWishlist = (produk_id) => async (dispatch) => {
  try {
    dispatch({ type: `${ADD_WISHLIST}_LOADING` });

    await axios({
      method: 'POST',
      url: wishlistUrl,
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
      data: { produk_id },
    }).then((res) => {
      dispatch({
        type: `${ADD_WISHLIST}_FULFILLED`,
        payload: { produk_id: produk_id },
      });
      if (!Cookies.get('token')) {
        message.error('Silahkan login terlebih dahulu');
        return false;
      }
      message.success('Berhasil ditambahkan ke wishlist');
      dispatch(getWishlist());
    });
  } catch (error) {
    dispatch({
      type: `${ADD_WISHLIST}_ERROR`,
      error: error,
    });
  }
};

export const deleteWishlist = (wishlist_id) => async (dispatch) => {
  try {
    dispatch({ type: `${DELETE_WISHLIST}_LOADING` });

    await axios({
      method: 'DELETE',
      url: wishlistUrl + '/delete',
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
      data: { wishlist_id },
    }).then((res) => {
      dispatch({
        type: `${DELETE_WISHLIST}_FULFILLED`,
        payload: { wishlist_id: wishlist_id },
      });
      dispatch(getWishlist());
      message.success('Wishlist berhasil dihapus');
    });
  } catch (error) {
    dispatch({
      type: `${DELETE_WISHLIST}_ERROR`,
      error: error,
    });
  }
};
