import axios from 'axios';
import {GET_ALL_PRODUCT, GET_MY_PRODUCT, GET_PRODUCT_BY_ID, GET_WISHLISHED_PRODUCT, PRODUCT} from '../type/ProductType';
import {headerApi, productUrl} from './config';
import Cookies from 'js-cookie';
import {message} from 'antd';

export const getAllProduct = () => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'GET',
            url: productUrl + '/all',
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
        }).then((res) => {
            dispatch({
                type: `${GET_ALL_PRODUCT}_FULFILLED`,
                payload: res.data.data,
            });
        });
    } catch (error) {
        dispatch({
            type: `${PRODUCT}_ERROR`,
            error: error,
        });
    }
};

export const searchProduct = (search) => async (dispatch) => {
    console.log(search, "ini sebelum get")
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'GET',
            url: productUrl + `/all?offset=0&limit=10&search=${search}`,
            headers: headerApi,
            data: {offset: 0, limit: 10, search: search}
        }).then((res) => {
            dispatch({
                type: `${GET_ALL_PRODUCT}_FULFILLED`,
                payload: res.data.data,
            });
            console.log(res.data.data, "ini di search")
        });
    } catch (error) {
        dispatch({
            type: `${PRODUCT}_ERROR`,
            error: error,
        });
    }
};

export const getMyProduct = () => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'GET',
            url: productUrl + '/mine',
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
        }).then((res) => {
            dispatch({
                type: `${GET_MY_PRODUCT}_FULFILLED`,
                payload: res.data.data,
            });
            console.log(res.data.data);
        });
    } catch (error) {
        dispatch({
            type: `${PRODUCT}_ERROR`,
            error: error,
        });
    }
};

export const getWishlishedProduct = () => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'GET',
            url: productUrl + '/wishlished',
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
        }).then((res) => {
            dispatch({
                type: `${GET_WISHLISHED_PRODUCT}_FULFILLED`,
                payload: res.data.data,
            });
        });
    } catch (error) {
        dispatch({
            type: `${PRODUCT}_ERROR`,
            error: error,
        });
    }
};

export const createProduct = (formData, step) => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'POST',
            url: productUrl,
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
            data: formData,
        }).then((res) => {
            dispatch({type: `${PRODUCT}_LOADING_FULFILLED`});
            if (res.data.message === 'Maksimum 4 produk aktif yang dijual') {
                message.error('Gagal upload, maksimum 4 produk aktif yang dijual');
            } else {
                Cookies.set('product_id', res.data.data.id);
                dispatch({
                    type: `STEP`,
                    payload: step + 1,
                });
            }
        });
    } catch (error) {
        dispatch({
            type: `ERROR`,
            error: error,
        });
    }
};

export const editProduct = (formData, step) => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'PUT',
            url: productUrl,
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
            data: formData,
        }).then((res) => {
            dispatch({type: `${PRODUCT}_LOADING_FULFILLED`});
            dispatch({
                type: `STEP`,
                payload: step + 1,
            });
            Cookies.remove('edit');
        });
    } catch (error) {
        dispatch({
            type: `ERROR`,
            error: error,
        });
    }
};

export const addImage = (formData, navigate) => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'POST',
            url: productUrl + '/add-image',
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
            data: formData,
        }).then((res) => {
            dispatch({
                type: `STEP`,
                payload: 0,
            });
            message.success("Berhasil membuat produk!")
        });
    } catch (error) {
        dispatch({
            type: `ERROR`,
            error: error,
        });
    }
};

export const deleteProduct = (id, navigate) => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'DELETE',
            url: productUrl,
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
            data: {product_id: id},
        }).then((res) => {
            // dispatch(getMyProduct())

            // dispatch({
            //   type: `${DELETE_PRODUCT}_FULFILLED`,
            //   payload: res.data.data,
            // });
            navigate('/seller/daftar-jual');
        });
    } catch (error) {
        dispatch({
            type: `ERROR`,
            error: error,
        });
    }
};

export const deleteImage = (id, navigate) => async (dispatch) => {
    try {
        await axios({
            method: 'DELETE',
            url: productUrl,
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
            data: {product_id: id},
        }).then((res) => {
            dispatch(getProductByID(Cookies.get('product_id')));
        });
    } catch (error) {
        dispatch({
            type: `ERROR`,
            error: error,
        });
    }
};

export const getProductByID = (id) => async (dispatch) => {
    try {
        dispatch({type: `${PRODUCT}_LOADING`});

        await axios({
            method: 'GET',
            url: productUrl + '/' + id,
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
        }).then((res) => {
            dispatch({
                type: `${GET_PRODUCT_BY_ID}_FULFILLED`,
                payload: res.data.data,
            });
        });
    } catch (error) {
        dispatch({
            type: `${GET_PRODUCT_BY_ID}_ERROR`,
            error: error,
        });
    }
};
