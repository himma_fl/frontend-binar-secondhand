import axios from 'axios';
import { categoryUrl, headerApi } from './config';
import { CATEGORY, GET_CATEGORY } from '../type/CategoryType';
import { message } from 'antd';
import Cookies from 'js-cookie';

export const getCategory = () => async (dispatch) => {
  try {
    dispatch({ type: `${CATEGORY}_LOADING` });

    await axios({
      method: 'GET',
      url: categoryUrl,
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
    }).then((res) => {
      dispatch({
        type: `${GET_CATEGORY}_FULFILLED`,
        payload: res.data.data,
      });
    });
  } catch (error) {}
};

export const createCategory = (name) => async (dispatch) => {
  try {
    dispatch({ type: `${CATEGORY}_LOADING` });

    console.log(name);
    await axios({
      method: 'POST',
      url: categoryUrl,
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
      data: { name: name },
    }).then((res) => {
      dispatch(getCategory());
      message.success('Berhasil menambah kategori!');
    });
  } catch (error) {
    dispatch({
      type: `ERROR`,
      error: error,
    });
  }
};
