import { message } from 'antd';
import axios from 'axios';
import Cookies from 'js-cookie';
import { GET_PROFILE } from '../type/UserType';
import { headerApi, userUrl } from './config';

export const getProfile = () => async (dispatch) => {
  try {
    dispatch({ type: `${GET_PROFILE}_LOADING` });

    await axios({
      method: 'GET',
      url: userUrl + '/profile',
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
    }).then((res) => {
      dispatch({
        type: `${GET_PROFILE}_FULFILLED`,
        payload: res.data.data,
      });
    });
  } catch (error) {
    dispatch({
      type: `${GET_PROFILE}_ERROR`,
      error: error,
    });
  }
};

export const editProfile = (data, navigate) => async (dispatch) => {
  try {
    await axios({
      method: 'PUT',
      url: userUrl,
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
      data: data,
    }).then((res) => {
      message.success('Berhasil memperbarui profil!');
      navigate('/');
    });
  } catch (error) {
    dispatch({
      type: `${GET_PROFILE}_ERROR`,
      error: message.error('Gagal memperbarui profil!'),
    });
  }
};
