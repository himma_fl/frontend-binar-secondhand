import axios from 'axios';
import Cookies from 'js-cookie';
import {GET_NOTIFICATION, NOTIFICATION, NOTIFICATION_ISREAD} from '../type/NotificationType';
import {headerApi, transactionUrl} from './config';
import {PRODUCT} from "../type/ProductType";

export const getNotification = () => async (dispatch) => {
    dispatch({type: `${NOTIFICATION}_LOADING`});

    try {
        await axios({
            method: 'GET',
            url: transactionUrl + '/notification',
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
        }).then((res) => {
            let result = res.data.data.map((item) => item.is_notif_read);
            let isRead = result.includes(false);
            dispatch({
                type: `${NOTIFICATION_ISREAD}_FULFILLED`,
                payload: isRead,
            });
            dispatch({
                type: `${GET_NOTIFICATION}_FULFILLED`,
                payload: res.data.data,
            });
        });
    } catch (error) {
        dispatch({
            type: `${NOTIFICATION}_ERROR`,
            error: error,
        });
    }
};

export const notificationRead = (trx_id) => async (dispatch) => {
    try {
        await axios({
            method: 'POST',
            url: transactionUrl + '/notification',
            headers: {Authorization: `Bearer ${Cookies.get('token')}`},
            data: {trx_id},
        }).then((res) => {
            dispatch({
                type: `${NOTIFICATION_ISREAD}_FULFILLED`,
                payload: false,
            });
        });
    } catch (error) {
        dispatch({
            type: `${NOTIFICATION}_ERROR`,
            error: error,
        });
    }
};
