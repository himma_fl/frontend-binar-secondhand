import { message } from 'antd';
import axios from 'axios';
import Cookies from 'js-cookie';
import { authUrl } from './config';

export const authLogin = (username, password, navigate) => async (dispatch) => {
  try {
    dispatch({ type: `LOGIN` });

    await axios({
      method: 'POST',
      url: authUrl + '/login',
      data: { username, password },
    }).then((res) => {
      dispatch({
        type: `LOGIN`,
        payload: res.data,
      });
      console.log(res.data.status);
      if (res.data.status === true) {
        Cookies.set('token', res.data.data);
        navigate('/');
        message.success('Berhasil login!');
      } else if (res.data.status === false){
        message.error('Login gagal!');
      }
    });
  } catch (error) {
    dispatch({
      type: `ERROR`,
      error: error,
    });
  }
};

export const authRegister = (username, password, alamat, no_hp, navigate) => async (dispatch) => {
  try {
    dispatch({ type: `LOADING` });

    await axios({
      method: 'POST',
      url: authUrl + '/register',
      data: { username, password, alamat, no_hp },
    }).then((res) => {
      dispatch({
        type: `REGISTER`,
        payload: res.data,
      });
      navigate('/login');
      message.success('Register success!');
    });
  } catch (err) {
    dispatch({
      type: `ERROR`,
      error: err.message,
    });
    message.error('Register failed!');
  }
};
