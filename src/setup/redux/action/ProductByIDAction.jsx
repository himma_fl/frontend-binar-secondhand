import axios from 'axios';
import Cookies from 'js-cookie';
import { GET_PRODUCT_BY_ID } from '../type/ProductByIDType';
import { productUrl, headerApi } from './config';

export const getProductByID = (id) => async (dispatch) => {
  try {
    dispatch({ type: `${GET_PRODUCT_BY_ID}_LOADING` });

    await axios({
      method: 'GET',
      url: productUrl + '/' + id,
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
    }).then((res) => {
      dispatch({
        type: `${GET_PRODUCT_BY_ID}_FULFILLED`,
        payload: res.data.data,
      });
      console.log(res.data.data);
    });
  } catch (error) {
    dispatch({
      type: `${GET_PRODUCT_BY_ID}_ERROR`,
      error: error,
    });
  }
};
