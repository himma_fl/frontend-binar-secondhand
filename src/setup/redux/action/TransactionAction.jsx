import axios from 'axios';
import Cookies from 'js-cookie';
import { BUY_PRODUCT, GET_NOTIFICATION, GET_TRANSACTION_HISTORY, TRANSACTION } from '../type/TransactionType';
import { headerApi, transactionUrl } from './config';
import {message} from "antd";

export const getTransactionHistory = () => async (dispatch) => {
  try {
    dispatch({ type: `${TRANSACTION}_LOADING` });

    await axios({
      method: 'GET',
      url: transactionUrl + '/history',
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
    }).then((res) => {
      dispatch({
        type: `${GET_TRANSACTION_HISTORY}_FULFILLED`,
        payload: res.data.data,
      });
      console.log(res);
    });
  } catch (error) {
    dispatch({
      type: `${TRANSACTION}_ERROR`,
      error: error,
    });
  }
};

export const buyProduct = (data, navigate) => async (dispatch) => {
  try {
    dispatch({ type: `${TRANSACTION}_LOADING` });

    await axios({
      method: 'POST',
      url: transactionUrl + '/buy',
      headers: { Authorization: `Bearer ${Cookies.get('token')}` },
      data: { products: data },
    }).then((res) => {
      dispatch({
        type: `${BUY_PRODUCT}_FULFILLED`,
        payload: res.data.data,
      });
      message.success("Berhasil membeli produk!")
    });
  } catch (error) {
    dispatch({
      type: `${TRANSACTION}_ERROR`,
      error: error,
    });
  }
};
